import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { AuthService } from "src/services/auth.service";
@Injectable()
export class BannedUserGuard extends AuthGuard('jwt') implements CanActivate {

  constructor(
    private readonly authService: AuthService,
  ) {
    super();
  }

  public async canActivate(ctx: ExecutionContext): Promise<boolean> {
    const baseActivation = await super.canActivate(ctx);
    if (!baseActivation) {
      return false;
    }
    const user = (ctx as any).user || ctx?.switchToHttp()?.getRequest().user;
    const username = user && user.username;

    const foundUser = await this.authService.findUserByName(username);

    if(foundUser.banEndDate) {
        return true;
    }
    return true;
  }

}