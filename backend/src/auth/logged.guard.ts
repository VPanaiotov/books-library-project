import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";


@Injectable()
export class LoggedGuard extends AuthGuard('jwt') implements CanActivate {

  constructor() {
    super();
  }

  public async canActivate(ctx: ExecutionContext): Promise<boolean> {
    
    const request = ctx.switchToHttp().getRequest();
    const token = request.headers.authorization?.slice(7);

    if(token) {
        return;
    }
    return true
    }
}