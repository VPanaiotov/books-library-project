import { Book } from '../models/books.entity';
import { UserRole } from '../models/enums/user-role';
import { Repository, createConnection, Connection } from 'typeorm';
import { User } from '../models/users.entity';
import * as bcrypt from 'bcrypt';
import { SetupDb } from '../models/setupdb.entity';
import { Review } from '../models/reviews.entity';


const seedStart = async (connection: Connection) => {
    const setupDbRepository = connection.manager.getRepository(SetupDb);
    const setupData = await setupDbRepository.find({});
  
    if (setupData.length) {
      throw new Error(`Data has been already set up`);
    }
  };
  
const seedUsers = async (connection: Connection) => {
    const usersRepository: Repository<User> = connection.manager.getRepository(User);
    const foundUsers = await usersRepository.find({});
    
    const newUsers = [{
        username: 'admin',
        password: await bcrypt.hash('admin123', 10),
        role: UserRole.Admin
    },
    {
        username: 'basic',
        password: await bcrypt.hash('basic456', 10),
        role: UserRole.Basic
    },
    {
        username: 'kokosa',
        password: await bcrypt.hash('kokosa789', 10),
        role: UserRole.Basic
    }];
    for (const user of newUsers) {
        if (foundUsers.some(u => u.username === user.username)) continue;
        await usersRepository.save(await usersRepository.create(user));
    }
}

const seedBooks = async (connection: Connection) => {
    const booksRepository: Repository<Book> = connection.manager.getRepository(Book);
    const foundBooks = await booksRepository.find({});

    const newBooks = [
        {
            title: "Assasins Creed",
            author: "Oliver Bowden"
        },
        {
            title: "Lord of the Rings",
            author: "J R R Tolkien"
        },
        {
            title: "Think and grow rich",
            author: "Napoleon Hill"
        }
    ];
    for (const book of newBooks) {
        if (foundBooks.some(b => b.title === book.title)) continue;
        else {
            await booksRepository.save(await booksRepository.create(book));
        }
    }
}

const seedBookUserRelation = async (connection: Connection) => {
    const usersRepository: Repository<User> = connection.manager.getRepository(User);
    const booksRepository: Repository<Book> = connection.manager.getRepository(Book);

    const kokosa = await usersRepository.findOne({username: 'kokosa'});
    if(!kokosa) {
        throw new Error('Something went wrong, user kokosa not found!');
    }
    const books = await booksRepository.find({
        relations: ['borrowedBy']
    });
    if(books.length < 1) {
        throw new Error('Something went wrong, no books found!');
    }

    for (const book of books) {
        if (book.borrowedBy) continue;
        book.borrowedBy = kokosa;
        await booksRepository.save(book);
    }
}

const seedReviews = async (connection: Connection) => {
    const reviewsRepository: Repository<Review> = connection.manager.getRepository(Review);
    const usersRepository: Repository<User> = connection.manager.getRepository(User);
    const booksRepository: Repository<Book> = connection.manager.getRepository(Book);

    const kokosa = await usersRepository.findOne({username: 'kokosa'});
    if(!kokosa) {
        throw new Error('Something went wrong, user kokosa not found!');
    }
    const books = await booksRepository.find({});
    if(books.length < 1) {
        throw new Error('Something went wrong, no books found!');
    }
    const foundReviews = await reviewsRepository.find({});

    const newReviews = [
        {
            content: "Amazing Book"
        },
        {
            content: "I dont like it"
        },
        {
            content: "Best book ever"
        }
    ];
    for (let i = 0; i < newReviews.length; i++) {
        if (foundReviews.some(r => r.content === newReviews[i].content)) continue;
        else {
            const review = await reviewsRepository.create(newReviews[i]);
            review.author = kokosa;
            review.book = books[i];
            await reviewsRepository.save(review);
        }
    }
}

const seedEnd = async (connection: Connection) => {
    const setupDbRepository = connection.manager.getRepository(SetupDb);
  
    await setupDbRepository.save({ message: 'Setup has been completed!'});
  };
  


const seed = async () => {
    console.log('Seed started!');
    const connection = await createConnection();
try {
    await seedStart(connection);
    await seedUsers(connection);
    await seedBooks(connection);
    await seedBookUserRelation(connection);
    await seedReviews(connection);
    await seedEnd(connection);
    
} catch (error) {
    console.log(error.message);
}

    console.log('Seed completed!');
    connection.close();
}