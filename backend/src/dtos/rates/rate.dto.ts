import { UserDTO } from "../users/user.dto";
import { BookDTO } from "../books/book.dto";


export class RateDTO {
    public id: number;
    public rate: number;
    public user: Partial<UserDTO>;
    public book: Partial<BookDTO>;

}