import { IsNotEmpty, IsNumber, Max, Min } from "class-validator";

export class CreateRateDTO {
    @IsNotEmpty()
    @IsNumber()
    @Min(1)
    @Max(5)
    public rate: number;
}