import { IsNotEmpty, IsString, Length } from "class-validator";

export class UpdateReviewDTO {
    @IsNotEmpty()
    @IsString()
    @Length(5, 50)
    public content: string;
}