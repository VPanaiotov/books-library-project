import { UserDTO } from './../users/user.dto';
import { BookDTO } from 'src/dtos/books/book.dto';
import { LikeDTO } from '../likes/likes.dto';
export class ReviewDTO {
    public id: number;
    public content: string;
    public createdOn: Date;
    public updatedOn: Date;
    public book: Partial<BookDTO>;
    public author: Partial<UserDTO>;
    public likes: Partial<LikeDTO>[];
}