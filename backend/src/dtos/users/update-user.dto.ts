import { IsNotEmpty, IsString, Length } from "class-validator";


export class UpdateUserDTO {
    @IsString()
    @IsNotEmpty()
    @Length(8, 20)
    public newPassword: string;

    @IsString()
    @IsNotEmpty()
    @Length(8, 20)
    public oldPassword: string;
}