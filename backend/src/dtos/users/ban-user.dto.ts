import { IsNotEmpty, IsNumber, IsPositive, IsString } from "class-validator";

export class BanUserDTO {
    @IsNotEmpty()
    @IsNumber()
    // @IsPositive()
    public period: number;

    @IsNotEmpty()
    @IsString()
    public description: string;
}