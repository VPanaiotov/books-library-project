import { ReviewDTO } from './../reviews/review.dto';
import { BookDTO } from 'src/dtos/books/book.dto';
import { UserRating } from 'src/models/enums/user-rating';

export class UserDTO {
    public id: number;
    public username: string;
    public avatarUrl: string;
    public readBooks: Partial<BookDTO>[];
    public borrowedBooks: Partial<BookDTO>[];
    public reviews: Partial<ReviewDTO>[];
    public points: number;
    public level: UserRating;

}