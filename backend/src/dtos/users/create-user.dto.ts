import { IsNotEmpty, IsString, Length } from "class-validator";

export class CreateUserDTO {
    @IsNotEmpty()
    @IsString()
    @Length(5, 25)
    public username: string;
    @IsNotEmpty()
    @IsString()
    @Length(8, 20)
    public password: string;
}