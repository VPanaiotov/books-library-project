import { IsNotEmpty, IsNumber, Min, Max } from "class-validator";

export class UpdateUserRoleDTO {
    @IsNotEmpty()
    @IsNumber()
    @Min(1)
    @Max(2)
    public role: number;
}