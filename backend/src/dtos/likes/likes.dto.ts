import { UserDTO } from './../users/user.dto';
import { ReviewDTO } from './../reviews/review.dto';
export class LikeDTO {
    public id: number;

    public review: Partial<ReviewDTO>;

    public user: Partial<UserDTO>;
}