import { IsNotEmpty, IsString, Length } from 'class-validator';
export class CreateBookDTO {
    @IsNotEmpty()
    @IsString()
    @Length(5, 50)
    public title: string;
    @IsNotEmpty()
    @IsString()
    @Length(5, 30)
    public author: string;
    @IsNotEmpty()
    @IsString()
    @Length(15, 250)
    public description: string;
}