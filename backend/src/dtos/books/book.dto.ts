import { ReviewDTO } from './../reviews/review.dto';
import { UserDTO } from './../users/user.dto';
import { Rate } from 'src/models/rates.entity';

export class BookDTO {
    public id: number;
    public title: string;
    public author: string;
    public description: string;
    public isUnlisted: boolean;
    public borrowedBy: Partial<UserDTO>;
    public readUsers: Partial<UserDTO>[];
    public reviews: Partial<ReviewDTO>[];
    public rates: Rate[];
    public coverUrl: string;
}