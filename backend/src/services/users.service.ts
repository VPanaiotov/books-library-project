import { SystemError } from './../error-handling/sytem-error.error';
import { UserDTO } from './../dtos/users/user.dto';
import { CreateUserDTO } from './../dtos/users/create-user.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/users.entity';
import { Repository } from 'typeorm';
import { TransformService } from './transform.service';
import * as bcrypt from 'bcrypt';
import { UpdateUserDTO } from 'src/dtos/users/update-user.dto';
import { FindService } from './find.service';
import { ReviewDTO } from 'src/dtos/reviews/review.dto';
import { UserRole } from 'src/models/enums/user-role';


@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        private readonly transformer: TransformService,
        private readonly findService: FindService,
    ) { }

    public async getUserReviews(userId: number, withDeleted = false): Promise<Partial<ReviewDTO>[]> {
        const user = withDeleted
        ? await this.findService.findUserOrFail(userId, 'reviews','reviews.likes')
        : await this.usersRepository.findOne({
            where: {
                id: userId,
                isDeleted: false
            },
            relations: ['reviews','reviews.likes', 'reviews.book', 'reviews.author']
        })

        if (!user) {
            throw new SystemError(`User with id ${userId} is not found!`, 404);
        }
        if(withDeleted) return user.reviews;
        return user.reviews.map((review) => this.transformer.toReviewDTO(review,true));
    }

    public async createUser(body: CreateUserDTO): Promise<Partial<UserDTO>> {
        const foundUser = await this.usersRepository.findOne({
            where: {
                username: body.username,
            }
        });
        if (foundUser) {
            throw new SystemError(`User ${foundUser.username} already exists!`, 405);
        }
        const newUser = this.usersRepository.create({
            username: body.username,
            password: await bcrypt.hash(body.password, 10),
        });
        return this.transformer.toUserDTO(await this.usersRepository.save(newUser), true);
    }

    public async getAllUsers(): Promise<Partial<UserDTO>[]> {
        return await this.usersRepository.find();
    }

    public async updateAvatar(userId: number, filename: string) {
        const foundUser = await this.findService.findUserOrFail(userId);
        foundUser.avatarUrl = filename;
        await this.usersRepository.save(foundUser);
        return this.transformer.toUserDTO(foundUser);
    }

    public async readUser(userId: number, isDeleted = false, isBanned = false): Promise<Partial<UserDTO>> {
        const foundUser = await this.usersRepository.findOne({
            where: {
                id: userId,
                // isDeleted: isDeleted,
            },
            relations: ['borrowedBooks', 'readBooks', 'reviews', 'rates']
        });

        if(!foundUser) {
            throw new SystemError(`User not found!`, 404);
        }

        // if (foundUser.banEndDate > new Date() && !isBanned) {
        //     throw new SystemError(`User ${foundUser.username} is banned!`, 405);
        // }
        return this.transformer.toUserDTO(foundUser);
    }

    public async changeRole(userId: number, role: UserRole): Promise< { message: string }> {
        const foundUser = await this.findService.findUserOrFail(userId);

        foundUser.role = role;

        await this.usersRepository.save(foundUser);

        return { message: `User role changed`}
    }
    public async updateUserPassword(body: UpdateUserDTO, userId: number): Promise<{ message: string }> {
        const foundUser = await this.findService.findUserOrFail(userId);
        const isPasswordValid = await bcrypt.compare(body.oldPassword, foundUser.password);
        if(!isPasswordValid) {
            throw new SystemError('Old password does not match!', 406);
        }
        const updatedUser = this.usersRepository.create({...foundUser, password: await bcrypt.hash(body.newPassword, 10)});
        await this.usersRepository.save(updatedUser);

        return {
            message: `User ${foundUser.username} updated!`
        };
    }

    public async deleteUser(id: number): Promise< { message: string} > {
        const foundUser = await this.findService.findUserOrFail(id);
        if(foundUser.isDeleted) {
            throw new SystemError(`User ${foundUser.username} is already deleted!`, 405);
        }
        foundUser.isDeleted = true;
        await this.usersRepository.save(foundUser);

        return {
            message: `User ${foundUser.username} deleted!`
        }
    }
    public async recoverUser(id: number): Promise< { message: string} > {
        const foundUser = await this.usersRepository.findOne(id);
        if(!foundUser.isDeleted) {
            throw new SystemError(`User ${foundUser.username} is not deleted!`, 405);
        }
        foundUser.isDeleted = false;
        await this.usersRepository.save(foundUser);

        return {
            message: `User ${foundUser.username} successfully recovered!`
        }
    }

    public async banUser(userId: number, period: number, description: string): Promise< { message: string } > {
        const foundUser = await this.findService.findUserOrFail(userId);

        if(period < 0) {
            foundUser.banEndDate = null;
            foundUser.banDescription = null;
        }
        else {
        foundUser.banEndDate = new Date(Date.now() + period);
        foundUser.banDescription = description;
        }

        await this.usersRepository.save(foundUser);

        return {
            message: foundUser.banEndDate ? `User ${foundUser.username} successfully banned until ${foundUser.banEndDate} with description: ${foundUser.banDescription}!` : 'User successfully unbanned!'
        }
    }

    public async unbanUser(userId: number): Promise< { message: string } > {
        const foundUser = await this.findService.findUserOrFail(userId);
        foundUser.banEndDate = null;
        foundUser.banDescription = null;
        await this.usersRepository.save(foundUser);

        return {
            message: 'User successfully unbanned!'
        }
    }
}
