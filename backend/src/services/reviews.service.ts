import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/models/users.entity';
import { Review } from 'src/models/reviews.entity';
import { TransformService } from './transform.service';
import { ReviewDTO } from 'src/dtos/reviews/review.dto';
import { Like } from 'src/models/likes.entity';
import { SystemError } from 'src/error-handling/sytem-error.error';
import { UserLevelService } from './user-level.service';
import { UpdateReviewDTO } from 'src/dtos/reviews/update-review.dto';
import { FindService } from './find.service';

@Injectable()
export class ReviewsService {

    constructor(
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Review) private readonly reviewsRepository: Repository<Review>,
        @InjectRepository(Like) private readonly likesRepository: Repository<Like>,
        private readonly transformer: TransformService,
        private readonly userLevelService: UserLevelService,
        private readonly findService: FindService,
    ) {}

    public async getReview(reviewId: number, withDeleted = false): Promise<Partial<ReviewDTO>> {
        const review: Review = withDeleted 
        ? await this.reviewsRepository.findOne({
            where: {
                id: reviewId
            },
            relations: ['likes', 'book', 'author']
        })
        : await this.findService.findReviewOrFail(reviewId, 'likes', 'book', 'author');

        if(!review) {
            throw new SystemError(`Review with id ${reviewId} does not exist!`, 404);
        }
        if(withDeleted) return review;

        return this.transformer.toReviewDTO(review);
    }
    
    public async deleteReview(reviewId: number, userId: number, notAdmin = true): Promise<Partial<ReviewDTO>> {

        const foundReview = await this.findService.findReviewOrFail(reviewId, 'likes', 'book', 'author', 'likes.user');
        
        if (notAdmin && foundReview.author.id !==userId) {
            throw new SystemError('You are not the author of the review!', 405);
        }
        if (foundReview.author.id === userId) {
            let foundUser = await this.usersRepository.findOne(userId);
            foundUser = this.userLevelService.changeLevels(foundUser, -5);
            await this.usersRepository.save(foundUser);
        }
        foundReview.isDeleted = true;
        return this.transformer.toReviewDTO(await this.reviewsRepository.save(foundReview), true);
    }

    public async updateReview(reviewId: number, newReview: UpdateReviewDTO, userId: number, notAdmin = true): Promise<Partial<ReviewDTO>> {
        const foundReview = await this.findService.findReviewOrFail(reviewId, 'likes', 'book', 'author', 'likes.user');

        if (notAdmin && foundReview.author.id !==userId) {
            throw new SystemError('You are not the author of the review!', 405);
        }
        foundReview.content = newReview.content;
        this.reviewsRepository.save(foundReview);
        return this.transformer.toReviewDTO(foundReview, true);
    }
    public async changeLikes(reviewId: number, userId: number): Promise<Partial<ReviewDTO>> {
        const foundReview = await this.findService.findReviewOrFail(reviewId, 'author', 'likes', 'likes.user');

        let foundUser = await this.findService.findUserOrFail(userId, 'likes');
        let authorOfReview = await this.usersRepository.findOne({
                where: {
                    id: foundReview.author.id,
                },
            });
        let foundLike;
        if (foundReview.likes.some(like => like.user.id === userId)) {

            const like = foundReview.likes.find((like) => like.user.id === userId);
            

            foundLike = await this.likesRepository.findOne({
                where: {
                    id: like.id
                }
            });
            foundLike.isDeleted = !foundLike.isDeleted;
        } else {
            foundLike = this.likesRepository.create({
                user: foundUser,
                review: foundReview
            });
        }
        foundUser = foundLike.isDeleted
                ? this.userLevelService.changeLevels(foundUser, -1)
                : this.userLevelService.changeLevels(foundUser, 1);
        authorOfReview = foundLike.isDeleted
                ? this.userLevelService.changeLevels(authorOfReview, -1)
                : this.userLevelService.changeLevels(authorOfReview, 1);
        foundUser.likes.push(await this.likesRepository.save(foundLike));
        await this.usersRepository.save(foundUser);
        if(foundReview.likes.find(like => like.id === foundLike.id)) {
            foundReview.likes.find(like => like.id === foundLike.id).isDeleted = foundLike.isDeleted;
        }
        else {
            foundReview.likes.push(foundLike);
        }
        await this.usersRepository.save(authorOfReview);
        return this.transformer.toReviewDTO(foundReview, true);
    }
}
