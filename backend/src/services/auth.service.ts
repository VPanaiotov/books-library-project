import { InjectRepository } from "@nestjs/typeorm";
import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { User } from "src/models/users.entity";
import { Repository } from "typeorm";
import { JWTPayload } from "src/common/jwt-payload";
import * as bcrypt from 'bcrypt';
import { UserLoginDTO } from "src/dtos/users/user-login.dto";
import { SystemError } from "src/error-handling/sytem-error.error";
import { Token } from "src/models/tokens.entity";
import { UserRating } from "src/models/enums/user-rating";
import { UserRole } from "src/models/enums/user-role";

@Injectable()
export class AuthService {

  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Token) private readonly tokensRepository: Repository<Token>,
    private readonly jwtService: JwtService,
  ){}

  public async findUserByName(username: string): Promise<User> {
    const user = await this.userRepository.findOne({ 
      where: {
        username,
      }
    });
    if(!user) {
      throw new SystemError(`User ${username} not found!`, 404);
    }
    if(user.isDeleted) {
      throw new SystemError(`User ${username} is deleted!`, 406);
    }
    return user;
  }

  public async validateUser(username: string, password: string): Promise<User|null> {
    const user = await this.findUserByName(username);
    if (!user) {
      return null;
    }
    const isUserValidated = await bcrypt.compare(password, user.password);
    return isUserValidated
            ? user
            : null;
  }

  public async login(loginUser: UserLoginDTO): Promise<{ token: string }> {
    const user = await this.validateUser(loginUser.username, loginUser.password);
    const foundUser = await this.userRepository.findOne({
      where: {
        username: loginUser.username
      }
    })
    if (!user) {
      throw new SystemError('Wrong credentials!', 401);
    }
    if (foundUser.banEndDate && Date.now() < foundUser.banEndDate.valueOf()) {
      throw new SystemError(`User is banned until ${foundUser.banEndDate}`, 403)
    }

    const payload: JWTPayload = {
      id: user.id,
      username: user.username,
      avatarUrl: user.avatarUrl,
      level: [UserRating[user.level], user.level],
      role: UserRole[user.role],
    }

    const token = await this.jwtService.signAsync(payload);

    return {
      token,
    };
  }
  public async blacklist(token:string): Promise<void> {
    const createToken = this.tokensRepository.create({token});
    if (await this.tokensRepository.findOne(createToken)) {
      throw new SystemError(`Already logged out`, 405);
    }
    
    await this.tokensRepository.save(createToken);
  }
  public async isBlacklisted(token:string): Promise<boolean> {

    return Boolean(await this.tokensRepository.findOne({
      where: {
        token: token
      }
    }));
  }
}

