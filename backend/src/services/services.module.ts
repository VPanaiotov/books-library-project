import { Token } from '../models/tokens.entity';
import { Review } from './../models/reviews.entity';
import { Book } from './../models/books.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { User } from 'src/models/users.entity';
import { UsersService } from './users.service';
import { BooksService } from './books.service';
import { TransformService } from './transform.service';
import { Like } from 'src/models/likes.entity';
import { ReviewsService } from './reviews.service';
import { Rate } from 'src/models/rates.entity';
import { PassportModule } from '@nestjs/passport'
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/constants/secrets';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategy/jwt-strategy';
import { UserLevelService } from './user-level.service';
import { FindService } from './find.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([User, Book, Review, Like, Rate, Token]),
        PassportModule,
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: {
                expiresIn: '1d',
            }
        }),

    ],
    providers: [
    UsersService,
    BooksService,
    TransformService,
    ReviewsService,
    AuthService,
    JwtStrategy,
    UserLevelService,
    FindService,
],
    exports: [
    UsersService,
    BooksService,
    ReviewsService,
    AuthService,
    ],
})
export class ServicesModule {}
