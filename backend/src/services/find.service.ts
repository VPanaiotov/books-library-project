import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Book } from 'src/models/books.entity';
import { SystemError } from 'src/error-handling/sytem-error.error';
import { User } from 'src/models/users.entity';
import { Review } from 'src/models/reviews.entity';

@Injectable()
export class FindService {
    constructor(
        @InjectRepository(Book) private readonly booksRepository: Repository<Book>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Review) private readonly reviewsRepository: Repository<Review>,
    ) { }
    async findBookOrFail(id: number, ...relations: string[]): Promise<Book> {
        const foundBook = await this.booksRepository.findOne(
            {
                where: {
                    id: id,
                    isUnlisted: false,
                },
                relations: [...relations],
            });
        if (!foundBook) {
            throw new SystemError(`Book with id ${id} does not exist!`, 404);
        }
        return foundBook;
    }

    async findUserOrFail(
        id: number,
        ...relations: string[]): Promise<User> {
        const user =  await this.usersRepository.findOne(
            {
                where: {
                    id: id,
                    isDeleted: false,
                },
                relations: [...relations],
            });
        if (!user) {
            throw new SystemError(`User with id ${id} does not exist!`, 404);
        }
        return user;
    }

    async findReviewOrFail(id: number, ...relations: string[]): Promise<Review> {
        const foundReview = await this.reviewsRepository.findOne(
            {
                where: {
                    id: id,
                    isDeleted: false,
                },
                relations: [...relations],
            });
        if (!foundReview) {
            throw new SystemError(`Review with id ${id} does not exist!`, 404);
        }
        return foundReview;
    }


}
