import { UserRating } from './../models/enums/user-rating';
import { User } from 'src/models/users.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UserLevelService {
    public changeLevels(user: User, points: number): User {
        user.points+=points;
        if(user.points > 1500) user.level = UserRating.Kokos;
        else if(user.points > 500) user.level = UserRating.HeavyReader;
        else if(user.points > 200) user.level = UserRating.Moderate;
        else if(user.points > 70) user.level = UserRating.Advancing;
        else user.level = UserRating.New;
        return user;
    }
}
