import { ReviewDTO } from './../dtos/reviews/review.dto';
import { Review } from './../models/reviews.entity';
import { UserDTO } from './../dtos/users/user.dto';
import { User } from './../models/users.entity';
import { BookDTO } from './../dtos/books/book.dto';
import { Book } from './../models/books.entity';
import { Injectable } from '@nestjs/common';
import { Like } from 'src/models/likes.entity';
import { LikeDTO } from 'src/dtos/likes/likes.dto';
import { Rate } from 'src/models/rates.entity';
import { RateDTO } from 'src/dtos/rates/rate.dto';
import { UserRating } from 'src/models/enums/user-rating';

@Injectable()
export class TransformService {
    toUserDTO(user: User, stop = false): Partial<UserDTO> {
        if(!user) {
            return {username: 'N/A'};
        }
        if (stop) {
            return {
                id: user.id,
                username: user.username,
                avatarUrl: user.avatarUrl,
                level: UserRating[user.level] as any,
            }
        }
        return {
            id: user.id,
            username: user.username,
            avatarUrl: user.avatarUrl,
            readBooks: user.readBooks ? user.readBooks.map(book => this.toBookDTO(book, true)) : [],
            borrowedBooks: user.borrowedBooks ? user.borrowedBooks.map(book => this.toBookDTO(book, true)) : [],
            points: user.points
        }
    }
    toBookDTO(book: Book, stop = false): Partial<BookDTO> {
        if(!book) return { author: 'N/A', title: 'N/A', description: 'N/A'};
        if (stop) {
            return {
                id: book.id,
                title: book.title,
                author: book.author,
                description: book.description,
                coverUrl: book.coverUrl,
            }
        }
        return {
            id: book.id,
            title: book.title,
            author: book.author,
            description: book.description,
            borrowedBy: book.borrowedBy ? this.toUserDTO(book.borrowedBy, true) : null,
            readUsers: book.readUsers ? book.readUsers.map(user => this.toUserDTO(user, true)) : [],
            reviews: book.reviews ? book.reviews.map(review => this.toReviewDTO(review, true)) : [],
            rates: book.rates,
            coverUrl: book.coverUrl,
        }
    }
    toReviewDTO(review: Review, stop = false): Partial<ReviewDTO> {
        if(!review) return { content: 'N/A' };
        if (stop) {
            return {
                id: review.id,
                content: review.content,
                createdOn: review.createdOn,
                updatedOn: review.updatedOn,
                likes: review.likes.filter((like) => !like.isDeleted).map((like) => this.toLikeDTO(like, true)),
                author: this.toUserDTO(review.author, true),

            }
        }
        return {
            id: review.id,
            content: review.content,
            createdOn: review.createdOn,
            updatedOn: review.updatedOn,
            book: this.toBookDTO(review.book, true),
            author: this.toUserDTO(review.author, true),
            likes: review.likes.filter((like) => !like.isDeleted).map((like) => this.toLikeDTO(like, true)),
        }
    }

    toLikeDTO(like: Like, stop = false): Partial<LikeDTO> {
        if (stop) {
            return {
                id: like.id,
                user: this.toUserDTO(like?.user, true)
            }
        }
        return {
            id: like.id,
            review: like.review,
            user: like.user,
        }
    }

    toRateDTO(rate: Rate, stop = false): Partial<RateDTO> {
        if (stop) {
            return {
                id: rate.id,
                rate: rate.rate,
            }
        }
        return {
            id: rate.id,
            rate: rate.rate,
            user: this.toUserDTO(rate.user, true),

            book: this.toBookDTO(rate.book, true)
        }
    }
}
