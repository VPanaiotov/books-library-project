import { Injectable } from "@nestjs/common";

import { PassportStrategy } from "@nestjs/passport";

import { AuthService } from "../auth.service";

import { ExtractJwt, Strategy } from "passport-jwt";

import { jwtConstants } from "src/constants/secrets";

import { JWTPayload } from "src/common/jwt-payload";
import { User } from "src/models/users.entity";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  async validate(payload: JWTPayload): Promise<User|false> {

    const user = await this.authService.findUserByName(payload.username);

    if (!user) {
      return;
    }

    if (user.banEndDate && Date.now() < user.banEndDate.valueOf()) {
      return;
    }

    return user;
  }
}
