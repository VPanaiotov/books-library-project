import { UserLevelService } from './user-level.service';
import { CreateReviewDTO } from './../dtos/reviews/create-review.dto';
import { ReviewDTO } from './../dtos/reviews/review.dto';
import { BookDTO } from 'src/dtos/books/book.dto';
import { Book } from 'src/models/books.entity';
import { CreateBookDTO } from './../dtos/books/create-book.dto';
import { TransformService } from './transform.service';
import { Review } from './../models/reviews.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/models/users.entity';
import { Rate } from 'src/models/rates.entity';
import { SystemError } from 'src/error-handling/sytem-error.error';
import { UpdateBookDTO } from 'src/dtos/books/update-book.dto';
import { FindService } from './find.service';
import { RateDTO } from 'src/dtos/rates/rate.dto';

@Injectable()
export class BooksService {
    constructor(
        @InjectRepository(Book) private readonly booksRepository: Repository<Book>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Review) private readonly reviewsRepository: Repository<Review>,
        @InjectRepository(Rate) private readonly rateRepository: Repository<Rate>,
        private readonly transformer: TransformService,
        private readonly userLevelService: UserLevelService,
        private readonly findService: FindService,
    ) {}
    public async getAllBooks(withUnlisted = false): Promise<Partial<BookDTO>[]> {
        const books: Book[] = withUnlisted
        ? await this.booksRepository.find({
            relations: ['borrowedBy', 'readUsers', 'reviews', 'reviews.likes', 'reviews.author', 'reviews.likes.user', 'rates']
        })
        : await this.booksRepository.find({
            where: { isUnlisted: false},
            relations: ['borrowedBy', 'readUsers', 'reviews', 'reviews.likes', 'reviews.author', 'reviews.likes.user', 'rates']
            });
        if(withUnlisted) return books;
        return books.map(book => this.transformer.toBookDTO(book));
    }
    public async getOneBook(bookId: number, withUnlisted = false): Promise<Partial<BookDTO>> {

        const book: Book = withUnlisted
        ? await this.booksRepository.findOne({
            where: {
                id: bookId
            },
            relations: ['borrowedBy', 'readUsers', 'reviews', 'reviews.likes', 'rates']
        })
        : await this.findService.findBookOrFail(bookId, 'borrowedBy', 'readUsers', 'reviews', 'reviews.likes', 'rates');

        if (!book) {
            throw new SystemError(`Book with id ${bookId} does not exist!`, 404);
        }

        if(!withUnlisted) book.reviews = book.reviews.filter(review => review.isDeleted === false);
        if(withUnlisted) return book;
        return this.transformer.toBookDTO(book);
    }
    public async createBook(book: CreateBookDTO): Promise<Partial<BookDTO>> {
        const foundBook: Book = await this.booksRepository.findOne({ title: book.title });
        if (foundBook) {
            throw new SystemError('Book with this title already exists!', 405);
        }

        const newBook: Book = this.booksRepository.create(
            {
                title: book.title,
                author: book.author,
                description: book.description
            }
        );


        return this.transformer.toBookDTO(await this.booksRepository.save(newBook));
    }

    public async borrowBook(bookId: number, userId: number): Promise<{message: string}> {

        const foundBook: Book = await this.findService.findBookOrFail(bookId, 'borrowedBy', 'readUsers', 'rates');
        if (foundBook.borrowedBy) {
            throw new SystemError(`The book is already borrowed!`, 405)
        }
        let foundUser: User = await this.findService.findUserOrFail(userId, 'borrowedBooks', 'readBooks');
        if (!foundUser.readBooks.some(book => book.id === foundBook.id)) {
            foundUser = this.userLevelService.changeLevels(foundUser, 10);
        }
        foundBook.readUsers.push(foundUser);
        foundBook.borrowedBy = foundUser;
        await this.booksRepository.save(foundBook);
        foundUser.readBooks.push(foundBook);
        foundUser.borrowedBooks.push(foundBook);
        await this.usersRepository.save(foundUser);
        return { message: `Book '${foundBook.title}' successfully borrowed!` };
    }

    public async returnBook(bookId: number, userId: number): Promise<{message: string}> {

        const foundBook: Book = await this.findService.findBookOrFail(bookId, 'borrowedBy', 'readUsers', 'rates');

        const foundUser: User = await this.findService.findUserOrFail(userId, 'borrowedBooks');
        if (!foundUser.borrowedBooks.some((book) => book.id === bookId)) {
                throw new SystemError('The book is not borrowed by you!', 406);
        }
        await this.booksRepository
            .createQueryBuilder()
            .update(Book)
            .set({ borrowedBy: null})
            .where('id = :id', { id: bookId })
            .execute();
        return { message: `Book '${foundBook.title}' successfully returned!` };
    }

    public async createReview(bookId: number, body: CreateReviewDTO, userId: number): Promise<{message: string}> {
        const book = await this.findService.findBookOrFail(bookId, 'readUsers', 'reviews', 'rates');
        if (!book.readUsers.some(user => user.id === userId)) {
            throw new SystemError(`You have not read the book, therefore cannot create a review!`, 405);
        }

        let foundUser = await this.findService.findUserOrFail(userId, 'reviews');
        foundUser = this.userLevelService.changeLevels(foundUser, 5);
        await this.usersRepository.save(foundUser);
        
        const review = this.reviewsRepository.create({
            content: body.content,
            author: foundUser,
            book: book,
            likes: [],
        });
        await this.reviewsRepository.save(review);

        return {message: `Review created`};
    }

    public async getBookReviews(bookId: number): Promise<Partial<ReviewDTO>[]> {

        const book = await this.findService.findBookOrFail(bookId, 'reviews', 'reviews.likes', 'reviews.author', 'reviews.likes.user');
        return book.reviews
        .filter(review => review.isDeleted === false)
        .map((review) => this.transformer.toReviewDTO(review, true));
    }

    public async rateBook(bookId: number, rate: number, userId: number): Promise<{message: string}> {

        const foundBook = await this.findService.findBookOrFail(bookId, 'rates', 'borrowedBy');
        let foundUser = await this.findService.findUserOrFail(userId, 'readBooks', 'rates', 'rates.book');

        if(!foundUser.readBooks.some((book) => book.id === bookId)) {
            throw new SystemError(`You cannot rate a book that you have not read!`, 405);
        }
        let returnMessage = { message: `You have rated book '${foundBook.title}' with ${rate}`};
        if(foundUser.rates.some((el) => el.book.id === bookId)) {
            returnMessage = { message: `You have updated your rate for book '${foundBook.title}' to ${rate}`}
            await this.rateRepository
                .createQueryBuilder()
                .update(Rate)
                .set({rate: rate})
                .where('userId = :userId && bookId = :bookId', { userId: userId, bookId: bookId})
                .execute();
        }
        else {
            foundUser = this.userLevelService.changeLevels(foundUser, 2);
            const newRate = this.rateRepository.create(
                {
                    rate: rate,
                    book: foundBook,
                    user: foundUser,
                });
            await this.rateRepository.save(newRate);
            foundUser.rates.push(newRate);
            await this.usersRepository.save(foundUser);
        }
        return returnMessage;
    }

    public async updateBook(id: number, body: UpdateBookDTO): Promise<{ message: string }> {
        let foundBook: Book = await this.findService.findBookOrFail(id);
        foundBook = {...foundBook, ...body};
        await this.booksRepository.save(foundBook);
        return {message: `Book updated`}
    }

    public async unlistBook(id: number): Promise<{ message: string }> {
        const foundBook: Book = await this.booksRepository.findOne(id);
        if (foundBook.borrowedBy) {
            throw new SystemError(`Cannot delete a book which is borrowed!`, 405);
        }
        foundBook.isUnlisted = !foundBook.isUnlisted;
        await this.booksRepository.save(foundBook);

        return { message: foundBook.isUnlisted ? `Book unlisted` : `Book recovered`}
    }

    public async getRates(bookId): Promise<Partial<RateDTO>[]> {
        const foundBook = await this.findService.findBookOrFail(bookId, 'rates', 'rates.user')
        return foundBook.rates.map(rate => this.transformer.toRateDTO(rate));
    }

    public async uploadCover(bookId: number, filename: string) {
        const foundBook = await this.findService.findBookOrFail(bookId);
        foundBook.coverUrl = filename;
        await this.booksRepository.save(foundBook);
        return this.transformer.toBookDTO(foundBook);
    }
}
