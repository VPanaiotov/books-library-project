import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn } from "typeorm";

@Entity('tokens')
export class Token {
    @PrimaryGeneratedColumn()
    public id: number;
    @Column({ nullable: false, unique: true})
    public token: string;
    @CreateDateColumn()
    public blacklistedOn: Date;
}