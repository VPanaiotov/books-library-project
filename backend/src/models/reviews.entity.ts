import { Book } from './books.entity';
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, OneToMany } from "typeorm";
import { User } from "./users.entity";
import { Like } from './likes.entity';

@Entity('reviews')
export class Review {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({ nullable: false })
    public content: string;

    @CreateDateColumn()
    public createdOn: Date;

    @UpdateDateColumn()
    public updatedOn: Date;

    @Column({ default: false })
    public isDeleted: boolean;

    @ManyToOne(() => Book, book => book.reviews)
    public book: Book;

    @ManyToOne(() => User, user => user.reviews)
    public author: User;

    @OneToMany(() => Like, like => like.review)
    public likes: Like[];

}