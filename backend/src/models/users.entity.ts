import { UserRating } from './enums/user-rating';
import { Book } from './books.entity';
import { Review } from './reviews.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToMany, JoinTable } from "typeorm";
import { Rate } from './rates.entity';
import { Like } from './likes.entity';
import { UserRole } from './enums/user-role';

@Entity('users')
export class User {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({ nullable: false, unique: true })
    public username: string;

    @Column({ nullable: false })
    public password: string;

    @Column({ nullable: true })
    public avatarUrl: string;
    
    @Column({ nullable: false, default: false })
    public isDeleted: boolean;

    @Column({default: 0})
    public points: number;

    @Column({type: 'enum',
            enum: UserRating,
            default: UserRating.New})
    public level: UserRating;

    @Column({
        type: 'enum',
        enum: UserRole,
        default: UserRole.Basic,
    })
    role: UserRole;
    
    @Column({
        nullable: true,
        default: null,
    })
    banEndDate: Date;

    @Column({
        nullable: true,
        default: '',
    })
    banDescription: string;

    @ManyToMany(() => Book, book => book.readUsers)
    @JoinTable()
    public readBooks: Book[];

    @OneToMany(() => Book, book => book.borrowedBy)
    public borrowedBooks: Book[];

    @OneToMany(() => Review, review => review.author)
    public reviews: Review[];

    @OneToMany(() => Rate, rate => rate.user)
    public rates: Rate[];

    @OneToMany(() => Like, like => like.user)
    public likes: Like[];


}