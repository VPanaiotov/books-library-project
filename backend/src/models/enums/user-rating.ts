export enum UserRating {
  New = 1,
  Advancing = 2,

  Moderate = 3,

  HeavyReader = 4,

  Kokos = 5,
}
