import { PrimaryGeneratedColumn, Entity, ManyToOne, Column } from "typeorm";
import { Book } from "./books.entity";
import { User } from "./users.entity";

@Entity('rates')
export class Rate {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public rate: number;

    @ManyToOne(() => Book, book => book.rates)
    public book: Book;

    @ManyToOne(() => User, user => user.rates)
    public user: User;
}