import { User } from './users.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToMany, ManyToOne } from "typeorm";
import { Review } from "./reviews.entity";
import { Rate } from './rates.entity';

@Entity('books')
export class Book {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({ nullable: false, unique: true, length: 50 })
    public title: string;

    @Column({ nullable: false, length: 30 })
    public author: string;

    @Column({ nullable: false, length: 250})
    public description: string;

    @Column({ default: false })
    public isUnlisted: boolean;

    @ManyToOne(() => User, user => user.borrowedBooks)
    public borrowedBy: User;

    @ManyToMany(() => User, user => user.readBooks)
    public readUsers: User[];

    @OneToMany(() => Review, review => review.book)
    public reviews: Review[];

    @OneToMany(() => Rate, rate => rate.book)
    public rates: Rate[];

    @Column({default: ''})
    public coverUrl: string;
    
}