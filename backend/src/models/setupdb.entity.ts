import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity('setupdb')
export class SetupDb {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public message: string;

    @Column()
    public date: Date;

}