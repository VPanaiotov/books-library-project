import { Review } from 'src/models/reviews.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { User } from "./users.entity";

@Entity('likes')
export class Like {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({ default: false })
    public isDeleted: boolean;

    @ManyToOne(() => Review, review => review.likes)
    public review: Review;

    @ManyToOne(() => User, user => user.likes)
    public user: User;
}