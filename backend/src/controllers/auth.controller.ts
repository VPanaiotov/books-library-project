import { Controller, Post, Body, Delete, UseGuards, Req} from '@nestjs/common';
import { AuthService } from 'src/services/auth.service';
import { UserLoginDTO } from 'src/dtos/users/user-login.dto';
import { LoggedGuard } from 'src/auth/logged.guard';
import { Request } from 'express';
import { BlacklistGuard } from 'src/auth/blacklist.guard';

@Controller('session')
export class AuthController {

  constructor(
    private readonly authService: AuthService,
  ) {}

  @Post()
  @UseGuards(LoggedGuard)
  public async login(@Body() user: UserLoginDTO): Promise<{ token: string }> {
    return await this.authService.login(user);
  }

  @Delete()
  @UseGuards(BlacklistGuard)
  public async logout(@Req() req: Request): Promise<{message: string}> {
    const token = req.headers.authorization;
    await this.authService.blacklist(token?.slice(7));
    return { message: `User successfully logged out!`};
  }

}
