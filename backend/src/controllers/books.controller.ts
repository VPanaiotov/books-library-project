
import { CreateRateDTO } from './../dtos/rates/create-rate.dto';
import { CreateReviewDTO } from './../dtos/reviews/create-review.dto';
import { BooksService } from './../services/books.service';
import { Controller, Get, HttpCode, HttpStatus, Query, Post, Body, Param, Put, Patch, UseGuards } from '@nestjs/common';
import { BookDTO } from 'src/dtos/books/book.dto';
import { UserId } from 'src/auth/user-id.decorator';
import { ReviewDTO } from 'src/dtos/reviews/review.dto';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { BannedUserGuard } from 'src/auth/ban-user.guard';
import { Rate } from 'src/models/rates.entity';
import { RateDTO } from 'src/dtos/rates/rate.dto';

@Controller('books')
export class BooksController {
    constructor(
        private readonly booksService: BooksService,
    ) {}

    @Get()
    @HttpCode(HttpStatus.OK)
    public async getAllBooks(@Query('title') title: string): Promise<Partial<BookDTO>[]> {
        const books: Partial<BookDTO>[] = await this.booksService.getAllBooks();
    
        if (title) {
            return books.filter(book =>
                        book.title.toLowerCase().includes(title.toLowerCase()) || book.author.toLowerCase().includes(title.toLowerCase())
                    );
        }
    
        return books;
    }
    @Get(':id')
    @HttpCode(HttpStatus.OK)
    public async getOneBook(@Param('id') bookId: string): Promise<Partial<BookDTO>> {
        return await this.booksService.getOneBook(+bookId);
    }

    @Put(':id')
    @UseGuards(BlacklistGuard, BannedUserGuard)
    public async borrowBook(@Param('id') id: string, @UserId() userId: number): Promise<{message: string}> {
        return await this.booksService.borrowBook(+id, userId);
    }

    @Patch(':id')
    @UseGuards(BlacklistGuard, BannedUserGuard)
    public async returnBook(@Param('id') id: string, @UserId() userId: number): Promise<{message: string}> {
        return await this.booksService.returnBook(+id, userId)
    }

    @Post(':id/reviews')
    @UseGuards(BlacklistGuard, BannedUserGuard)
    public async createReview(@Param('id') id: string, @Body() body: CreateReviewDTO, @UserId() userId: number): Promise<{ message: string }>  {
        return await this.booksService.createReview(+id, body, userId);
    }

    @Get(':id/reviews')
    public async getReviews(@Param('id') id: string): Promise<Partial<ReviewDTO>[]> {
        return await this.booksService.getBookReviews(+id);
    }

    @Get(':id/rates')
    // @UseGuards(BlacklistGuard, BannedUserGuard)
    public async getRates(@Param('id') id): Promise<Partial<RateDTO>[]> {
        return this.booksService.getRates(+id);
    }

    @Put(':id/rates')
    @UseGuards(BlacklistGuard, BannedUserGuard)
    public async rateBook(@Param('id') id: string, @Body() body: CreateRateDTO, @UserId() userId: number): Promise<{message: string}> {
        return this.booksService.rateBook(+id, body.rate, userId);
    }

}
