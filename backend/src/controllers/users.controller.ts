import { CreateUserDTO } from './../dtos/users/create-user.dto';
import { Controller, Post, Body, Get, Put, Delete, UseGuards, UseInterceptors, UploadedFile, Param } from '@nestjs/common';
import { UsersService } from 'src/services/users.service';
import { UserDTO } from 'src/dtos/users/user.dto';
import { UserId } from 'src/auth/user-id.decorator';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { UpdateUserDTO } from 'src/dtos/users/update-user.dto';
import { LoggedGuard } from 'src/auth/logged.guard';
import { ReviewDTO } from 'src/dtos/reviews/review.dto';
import { BannedUserGuard } from 'src/auth/ban-user.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';

@Controller('users')
export class UsersController {

    constructor(
        private readonly userService: UsersService,
    ) { }

    @Get()
    @UseGuards(BlacklistGuard)
    public async getUser(@UserId() id: number): Promise<Partial<UserDTO>> {
        return await this.userService.readUser(id);
    }

    @Post()
    @UseGuards(LoggedGuard)
    public async createUser(@Body() user: CreateUserDTO): Promise<Partial<UserDTO>> {
        return await this.userService.createUser(user);
    }

    @Post(':id/avatar')
    @UseGuards(BlacklistGuard)
    @UseInterceptors(FileInterceptor('files', {
        storage: diskStorage({
            destination: './images/avatars',
            filename: (req, file, cb) => {
                // Generating a 32 random chars long string
                const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');
                //Calling the callback passing the random name generated with the original extension name
                cb(null, `${randomName}${extname(file.originalname)}`);
            }
        })
    }))
    public async uploadAvatar(@Param('id') id: string, @UploadedFile() files) {
        return await this.userService.updateAvatar(+id, files.filename);
    }

    @Put()
    @UseGuards(BlacklistGuard, BannedUserGuard)
    public async updateUserPassword(@Body() body: UpdateUserDTO, @UserId() userId: number): Promise<{ message: string}> {
        return await this.userService.updateUserPassword(body, userId);
    }

    @Delete()
    @UseGuards(BlacklistGuard, BannedUserGuard)
    public async deleteUser(@UserId() userId: number): Promise<{ message: string }> {
        return this.userService.deleteUser(userId);
    }

    @Get('reviews')
    @UseGuards(BlacklistGuard, BannedUserGuard)
    public async getAllReviews(@UserId() userId: number): Promise<Partial<ReviewDTO>[]> {
        return await this.userService.getUserReviews(userId);
    }
}
