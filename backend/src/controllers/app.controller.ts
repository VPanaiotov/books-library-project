import { Controller, Get, Redirect, HttpStatus } from '@nestjs/common';

@Controller()
export class AppController {

  @Get()
  @Redirect('/books', HttpStatus.MOVED_PERMANENTLY)
  getHello(): void {
    //redirect
  }
}
