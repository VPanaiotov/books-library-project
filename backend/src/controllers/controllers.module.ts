import { ServicesModule } from './../services/services.module';
import { AppController } from './app.controller';
import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { BooksController } from './books.controller';
import { ReviewsController } from './reviews.controller';
import { AuthController } from './auth.controller';
import { AdminController } from './admin.controller';

@Module({
    imports: [
        ServicesModule,
    ],
    controllers: [
        AppController,
        UsersController,
        BooksController,
        ReviewsController,
        AuthController,
        AdminController,
    ]
})
export class ControllersModule {}
