import { Controller, Get, UseGuards, Post, Body, Put, Delete, Param, Patch, Query, UseInterceptors, UploadedFile } from '@nestjs/common';
import { UsersService } from 'src/services/users.service';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { UserDTO } from 'src/dtos/users/user.dto';
import { CreateUserDTO } from 'src/dtos/users/create-user.dto';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/models/enums/user-role';
import { BanUserDTO } from 'src/dtos/users/ban-user.dto';
import { ReviewDTO } from 'src/dtos/reviews/review.dto';
import { ReviewsService } from 'src/services/reviews.service';
import { CreateBookDTO } from 'src/dtos/books/create-book.dto';
import { BookDTO } from 'src/dtos/books/book.dto';
import { BooksService } from 'src/services/books.service';
import { UpdateReviewDTO } from 'src/dtos/reviews/update-review.dto';
import { UpdateBookDTO } from 'src/dtos/books/update-book.dto';
import { UpdateUserRoleDTO } from 'src/dtos/users/update-user-role.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';

@Controller('admin')
export class AdminController {
    constructor(
    private readonly usersService: UsersService,
    private readonly reviewsService: ReviewsService,
    private readonly booksService: BooksService,
    ) { }

    @Get('users')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async getAllUser(): Promise<Partial<UserDTO>[]> {
        return this.usersService.getAllUsers();
    }

    @Get('users/:id')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async getUser(@Param('id') id: string): Promise<Partial<UserDTO>> {

        return this.usersService.readUser(+id, true, true);
    }

    @Post('users')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async createUser(@Body() user: CreateUserDTO): Promise<Partial<UserDTO>> {
        return await this.usersService.createUser(user);
    }

    @Put('users/:id')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    async updateUser(@Body() body: UpdateUserRoleDTO, @Param('id') userId: string): Promise<{ message: string}> {
        return await this.usersService.changeRole(+userId, body.role);
    }

    @Delete('users/:id')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async deleteUser(@Param('id') id: string): Promise<{ message: string }> {
        return this.usersService.deleteUser(+id)
    }
    @Patch('users/:id')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async recoverUser(@Param('id') id: string): Promise<{ message: string }> {
        return this.usersService.recoverUser(+id)
    }

    @Post('users/:id/ban')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async banUser(
        @Body() body: BanUserDTO,
        @Param('id') userId: string): Promise< { message: string } > {
        return this.usersService.banUser(+userId, body.period, body.description);
    }

    @Delete('users/:id/ban')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async unbanUser(
        @Param('id') userId: string): Promise< { message: string } > {
        return this.usersService.unbanUser(+userId);
    }


    @Get('users/:userId/reviews/')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async getAllReviews(@Param('userId') userId: string): Promise<Partial<ReviewDTO>[]> {
        return await this.usersService.getUserReviews(+userId, true);
    }

    @Get('reviews/:reviewId')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async getReview(@Param('reviewId') reviewId: string): Promise<Partial<ReviewDTO>> {
        return await this.reviewsService.getReview(+reviewId, true);
    }

    @Put('reviews/:reviewId')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async updateReviews(
        @Param('reviewId') reviewId: string,
        @Body() body: UpdateReviewDTO,
        ): Promise<Partial<ReviewDTO>> {
        return await this.reviewsService.updateReview(+reviewId, body, NaN, false);
    }

    @Delete('reviews/:reviewId')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async deleteReview(
        @Param('reviewId') reviewId: string,
        ): Promise<Partial<ReviewDTO>> {
        return await this.reviewsService.deleteReview(+reviewId, NaN, false);
    }

    @Get('books')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async getAllBooks(@Query('name') name: string): Promise<Partial<BookDTO>[]> {
        const books: Partial<BookDTO>[] = await this.booksService.getAllBooks(true);
    
        if (name) {
            return books.filter(book =>
                        book.title.toLowerCase().includes(name.toLowerCase()),
                    );
        }
    
        return books;
    }

    @Get('books/:id')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async getOneBook(@Param('id') bookId: string): Promise<Partial<BookDTO>> {
        return await this.booksService.getOneBook(+bookId, true);
    }

    @Post('books')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async createBook(@Body() body: CreateBookDTO): Promise<Partial<BookDTO>> {
        return await this.booksService.createBook(body);
    }

    @Put('books/:id')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async updateBook(
        @Body() body: UpdateBookDTO,
        @Param('id') id: string
        ): Promise<{message: string}> {
        return await this.booksService.updateBook(+id, body);
    }

    @Delete('books/:id')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async unlistBook(
        @Param('id') id: string
        ): Promise<{ message: string }> {
        return await this.booksService.unlistBook(+id);
    }

    @Post('books/:id/cover')
    @UseGuards(BlacklistGuard)
    @UseInterceptors(FileInterceptor('files', {
        storage: diskStorage({
            destination: './images/book-covers',
            filename: (req, file, cb) => {
                // Generating a 32 random chars long string
                const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');
                //Calling the callback passing the random name generated with the original extension name
                cb(null, `${randomName}${extname(file.originalname)}`);
            }
        })
    }))
    public async uploaduploadCover(@Param('id') id: string, @UploadedFile() files) {
        return this.booksService.uploadCover(+id, files.filename);
    }
}

