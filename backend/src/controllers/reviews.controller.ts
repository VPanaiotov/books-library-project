import { ReviewsService } from './../services/reviews.service';
import { Controller, Put, Param, Body, Delete, Get, UseGuards } from '@nestjs/common';
import { ReviewDTO } from 'src/dtos/reviews/review.dto';
import { LikeDTO } from 'src/dtos/likes/likes.dto';
import { UserId } from 'src/auth/user-id.decorator';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { UpdateReviewDTO } from 'src/dtos/reviews/update-review.dto';
import { BannedUserGuard } from 'src/auth/ban-user.guard';

@Controller('reviews')
export class ReviewsController {
    constructor(
        private readonly reviewsService: ReviewsService, 
    ) {}

    @Get(':reviewId')
    public async getReview(@Param('reviewId') reviewId: string): Promise<Partial<ReviewDTO>> {
        return await this.reviewsService.getReview(+reviewId);
    }

    @Put(':reviewId')
    @UseGuards(BlacklistGuard, BannedUserGuard)
    public async updateReviews(
        @Param('reviewId') reviewId: string,
        @UserId() userId: number,
        @Body() body: UpdateReviewDTO,
        ): Promise<Partial<ReviewDTO>> {
        return await this.reviewsService.updateReview(+reviewId, body, userId);
    }

    @Delete(':reviewID')
    @UseGuards(BlacklistGuard, BannedUserGuard)
    public async deleteReview(
        @Param('reviewID') reviewID: string,
        @UserId() userId: number
        ): Promise<Partial<ReviewDTO>> {
        return await this.reviewsService.deleteReview(+reviewID, userId);
    }

    @Put(':id/votes')
    @UseGuards(BlacklistGuard, BannedUserGuard)
    public async changeLikes(@Param('id') id: string, @UserId() userId: number): Promise<Partial<LikeDTO>> {
        return await this.reviewsService.changeLikes(+id, userId);
    }
}
