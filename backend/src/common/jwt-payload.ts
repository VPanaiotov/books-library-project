export class JWTPayload {
    id: number;
    username: string;
    level: [string, number];
    role: string;
    avatarUrl: string;
}
