import { join } from 'path';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { SystemErrorFilter } from './error-handling/system-error.filter';
import * as express from 'express';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(express.static(join(process.cwd(), './images/avatars')));
  app.use(express.static(join(process.cwd(), './images/book-covers')));
  app.use(express.static(join(process.cwd(), './images/website')));
  app.useGlobalPipes(new ValidationPipe({whitelist: true}));
  app.useGlobalFilters(new SystemErrorFilter());
  app.enableCors()

  await app.listen(3000);
}
bootstrap();
