import { SystemError } from "./sytem-error.error";
import { Catch, ExceptionFilter, ArgumentsHost, HttpStatus } from "@nestjs/common";

@Catch(SystemError)
export class SystemErrorFilter implements ExceptionFilter {
  public catch(exception: SystemError, host: ArgumentsHost): {error : string, status: string} {
    return host.switchToHttp().getResponse().status(exception.errorCode).json({
        error: HttpStatus[exception.errorCode],
        status: exception.message,
    })
  }
}