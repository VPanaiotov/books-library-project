Title: Learning Javascript, 3rd Edition
Author: Ethan Brown
Description: You will learn how to create powerful and responsive web applications on the client, or with Node.js on the server.


Title: Think and Grow rich
Author: Napoleon Hill
Description: Think and Grow Rich, based on the author’s famed Law of Success, represents the distilled wisdom of distinguished men of great wealth and achievement. 


Title: Rich Dad Poor Dad
Author: Robert Kiyosaki
Description: Robert tells the story of a boy with two dads to help you develop the mindset and financial knowledge you need to build a life of wealth and freedom.


Title: Thinking, Fast and Slow
Author: Daniel Kahneman
Description: There have been many good books on human rationality and irrationality, but only one masterpiece - Daniel Kahneman's Thinking, Fast and Slow.


Title: Thinking in Bets
Author: Annie Duke
Description: Poker champion turned business consultant Annie Duke teaches you how to get comfortable with uncertainty and make better decisions as a result.


Title: The Intelligent Investor
Author: Benjamin Graham
Description: The book explains value investing and how to generate longterm profits by ignoring the current market and picking companies with high intrinsic value.


Title: The Warren Buffett Way
Author: Robert Hagstrom
Description: The book outlines the business and investment principles of value investing practiced by American businessman and investor Warren Buffett.


Title: War and Peace
Author: Leo Tolstoy
Description: This study of early 19th-century Russian society is generally regarded as a masterwork of Russian literature and one of the world’s greatest novels.


Title: The Black Swan
Author: Nassim Taleb
Description: A black swan is a highly improbable event with three principal characteristics: it is unpredictable, carries a massive impact and after the fact, we concoct an explanation that makes it appear less random, and more predictable, than it was.


Title: Assassin's Creed: Underworld
Author: Oliver Bowden
Description: In Victorian era London, a disgraced Assassin goes deep undercover in a quest for redemption in this novel based on the Assassin’s Creed™ video game series.


Title: World of Warcraft: War Crimes
Author: Christie Golden
Description: The brutal siege of Orgrimmar is over. Garrosh Hellscream, one of the most reviled figures on Azeroth, was stripped of his title as warchief. His thirst for conquest devastated cities, nearly tore the Horde apart, and destroyed countless lives.


Title: World of Warcraft: Arthas - Rise of the Lich King
Author: Christie Golden
Description: When a plague of undeath threatened all that he loved, Arthas had to pursue a quest for a runeblade powerful enough to save his homeland. Arthas's path would lead him toward the Frozen Throne, where he would face the darkest of destinies.


