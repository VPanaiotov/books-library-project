import React, { useState, useContext } from "react";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { Button } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import AllReviews from "../../Review/AllReviews/AllReviews";
import Rate from "../../Rate/Rate";
import UserContext from "../../../providers/UserContext";
import { BASE_URL } from "../../../common/constants";
import * as toastr from "toastr";
import BookDescription from "./Description";

const useStylesModal = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "900px",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

function TransitionsModal(book, history, borrowedBooksPage, setBorrowedBooksPage) {
  const classes = useStylesModal();
  const [open, setOpen] = useState(false);
  const { user } = useContext(UserContext);
  const [borrowed, setBorrowed] = useState(book.borrowedBy);

  const [readBook, setReadBook] = useState(
    book.readUsers.some((bookUser) => bookUser.id === user?.id)
  );

  const handleOpen = () => {
    // history.push(`?id=${book.id}`)
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const borrowBook = () => {
    fetch(`${BASE_URL}/books/${book.id}`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
        "Content-Type": "application/json",
      },
    })
      .then((r) => r.json())
      .then((result) => {
        if (result.error) {
          return toastr.error(result.status);
        }
        toastr.success(result.message);
        setBorrowed(user);
        setReadBook(true);
      })
      .catch((error) => toastr.error(error.message));
  };

  const returnBook = () => {
    fetch(`${BASE_URL}/books/${book.id}`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
        "Content-Type": "application/json",
      },
    })
      .then((r) => r.json())
      .then((result) => {
        if (result.error) {
          return toastr.error(result.status);
        }
        toastr.success(result.message);
        setBorrowed(null);
        setBorrowedBooksPage(!borrowedBooksPage);
      })
      .catch((error) => toastr.error(error.message));
  };

  const [showReviews, setShowReviews] = useState(false);

  const toggleReviews = () => {
    setShowReviews(!showReviews);
  }
  return (
    <>
      <Button
        variant="outlined"
        color="primary"
        size="small"
        onClick={handleOpen}
        style={{ fontSize: "12px", width: "100px", height: "30px" }}
      >
        More info
      </Button>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
        style={{ margin: "auto" }}
      >
        <Fade in={open}>
          <div id="book-dialog-id" className={classes.paper}>
            <React.Fragment>
              <Typography color="textSecondary" variant="h6" gutterBottom>
                {book.title}
                {/* <Button>Reviews</Button> */}
              </Typography>
              <Grid container spacing={2}>
                <Grid item xs={4}>
                  <img
                    src={`${BASE_URL}/${book.coverUrl}`}
                    alt={'NO COVER'}
                    style={{ height: "400px" }}
                  />
                  <br />
                  {!user ? null : borrowed ? (
                    user.id === borrowed.id ? (
                      <Button
                        variant="outlined"
                        color="primary"
                        size="small"
                        onClick={returnBook}
                      >
                        Return the book
                      </Button>
                    ) : null
                  ) : (
                    <Button
                      variant="outlined"
                      color="primary"
                      size="small"
                      onClick={borrowBook}
                    >
                      Borrow the book
                    </Button>
                  )}
                  <Rate id={book.id} readBook={readBook}/>
                  <div style={{width: '560px'}}>
                    <Button
                      variant="outlined"
                      color="primary"
                      size="small"
                      onClick={() => toggleReviews()}>
                        {showReviews && 'Description' || 'Reviews'}
                      </Button>
                      </div>
                </Grid>
                <Grid item xs={6}>
                  <Typography
                    variant="body1"
                    color="textSecondary"
                    component="div"
                  >
                    {showReviews
                    ?<AllReviews book={book} readBook={readBook} />
                    : <BookDescription book={book}/>}
                  </Typography>
                </Grid>
                <Grid item sx={6}>
                  <Typography
                    variant="body1"
                    color="textSecondary"
                    component="p"
                  >
                    <b>Author:</b> {book.author}
                  </Typography>
                </Grid>
              </Grid>
            </React.Fragment>
          </div>
        </Fade>
      </Modal>
    </>
  );
}

export default TransitionsModal;
