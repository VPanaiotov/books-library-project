import React, { useState } from "react";
import propTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import { withRouter } from "react-router-dom";
import { Button } from "@material-ui/core";
import "./noFocus.css";
import TransitionsModal from './Modal';
import { BASE_URL } from "../../../common/constants";

const SingleBook = ({ book, history, borrowedBooksPage, setBorrowedBooksPage }) => {
  const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 0,
      paddingTop: "170%",
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: "rotate(180deg)",
    },
    avatar: {
      backgroundColor: red[500],
    },
  }));

  const classes = useStyles();


  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <Avatar aria-label="book" className={classes.avatar}>
            {book.title.split("")[0]}
          </Avatar>
        }
        title={book.title}
        subheader={book.author}
      />
      <img
      src={`${BASE_URL}/${book.coverUrl}`}
        alt={'NO COVER'}
        style={{ height: "400px" }}
      />
      <CardActions disableSpacing>
        {TransitionsModal(book, history, borrowedBooksPage, setBorrowedBooksPage)}
      </CardActions>
    </Card>
  );
};

SingleBook.propTypes = {
  book: propTypes.object.isRequired,
};

export default withRouter(SingleBook);
