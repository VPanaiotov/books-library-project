import React from 'react';
import Paper from '@material-ui/core/Paper';

const BookDescription = (props) => {
    return (
        <>
        <h1>Description</h1>
        <Paper>
            
            <p>{props.book.description}</p>
        </Paper>
        </>
    );
}

export default BookDescription