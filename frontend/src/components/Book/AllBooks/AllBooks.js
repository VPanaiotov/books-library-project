import React, { useState, useEffect, useContext } from 'react';
import SingleBook from './../SingleBook/SingleBook';
import { BASE_URL } from '../../../common/constants';
import * as toastr from 'toastr';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import UserContext from '../../../providers/UserContext';

const AllBooks = (props) => {

    const [books, setBooks] = useState([]);
    const { user } = useContext(UserContext);
    const [borrowedBooksPage, setBorrowedBooksPage] = useState(false);
    
    useEffect(() => {
        // setLoading(true);
        fetch(`${BASE_URL}/books${props.location.search || ''}`)
          .then(response => response.json())
          .then(result => {
            if (Array.isArray(result) && result.length !==0) {
              if(props.location.pathname.includes('borrowed')) {
                result = result.filter((book) => book.borrowedBy?.id === user.id);
              }
              setBooks(result);
            } else if (props.location.pathname === '/books') {
              setBooks([]);
              return toastr.info('No books in the database!');
            }
            else {
              setBooks([]);
              return toastr.info('No matching books found!');
            }
          })
          .catch(error => toastr.error(error.message))
          .finally();
      }, [props.location.search, props.location.pathname, borrowedBooksPage]);
    
      const useStyles = makeStyles((theme) => ({
        root: {
          flexGrow: 1,
          paddingTop: '75px'
        },
        paper: {
          padding: theme.spacing(1),
          textAlign: 'center',
          color: theme.palette.text.secondary,
        },
      }));
  
        const classes = useStyles();
      
        return (
          <div className={classes.root}>
            <Grid container spacing={2}>
              <Grid container item xs={12} spacing={3}>
              {books.length !== 0 
              ? books.sort((a,b) => (a.title - b.title) ? -1 : 1).map(book => 
                      <Grid item xs={2} key={book.id}>
                        <SingleBook book={book} borrowedBooksPage={borrowedBooksPage} setBorrowedBooksPage={setBorrowedBooksPage} />
                      </Grid>) 
              : <h1 style={{marginLeft: '30px'}}>No books to display</h1>}

              </Grid>
            </Grid>
          </div>
        );
};

export default AllBooks;