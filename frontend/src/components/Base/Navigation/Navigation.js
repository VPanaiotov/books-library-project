import React, { useContext, useState } from "react";
import { fade, makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import InputBase from "@material-ui/core/InputBase";
import Badge from "@material-ui/core/Badge";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import SearchIcon from "@material-ui/icons/Search";
import AccountCircle from "@material-ui/icons/AccountCircle";
import NotificationsIcon from "@material-ui/icons/Notifications";
import { withRouter } from "react-router-dom";
import * as toastr from "toastr";
import Login from "../../pages/SignIn/Login";
import UserContext from "../../../providers/UserContext";
import { BASE_URL } from "./../../../common/constants";
import { Avatar, Button } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import clsx from "clsx";
import Divider from "@material-ui/core/Divider";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import AccountCircleRoundedIcon from "@material-ui/icons/AccountCircleRounded";
import HomeRoundedIcon from "@material-ui/icons/HomeRounded";
import LocalLibraryRoundedIcon from "@material-ui/icons/LocalLibraryRounded";
import MenuBookRoundedIcon from "@material-ui/icons/MenuBookRounded";
import GradeRoundedIcon from "@material-ui/icons/GradeRounded";
import PersonIcon from "@material-ui/icons/Person";
import SupervisorAccountIcon from "@material-ui/icons/SupervisorAccount";
import { Styles } from "../../pages/SignIn/Styles";
import { Formik, useField, Form } from "formik";
import * as Yup from "yup";

import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import Modal from "@material-ui/core/Modal";

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
}));

const drawerWidth = 240;

const useStylesMenu = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));

const useStylesRightMenu = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginRight: drawerWidth,
  },
  title: {
    flexGrow: 1,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-start",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginRight: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginRight: 0,
  },
}));

const useModalStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const Navigation = (props) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
  const { user, setUser } = useContext(UserContext);

  const modalClasses = useModalStyles();
  const [modalOpen, setModalOpen] = useState(false);
  const handleModalOpen = (event) => {
      setModalOpen(true);
  };
  const handleModalClose = () => {
    setModalOpen(false);
  };

  const [showUploadFields, setshowUploadFields] = useState(false);
  const [files, setFiles] = useState([]);
  const uploadAvatar = (id) => {
    const formData = new FormData();

    if (!files.length) {
      return;
    }

    formData.append('files', files[0]);
    fetch(`${BASE_URL}/users/${id}/avatar`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token') || ''}`
      },
      body: formData
    })
    .then(r => r.json())
    .then(result => {
      if (result.error) {
        return toastr.error(result.message);
      }
      setUser({
        ...user,
        avatarUrl: result.avatarUrl
      })
      toastr.success(!user?.avatarUrl ? 'Avatar successfully uploaded!' : 'Avatar successfully changed!');
    })
    .catch(error => toastr.error(error.message));
  };

  const [openLeftMenu, setOpenLeftMenu] = React.useState(false);
  const classesMenu = useStylesMenu();
  const theme = useTheme();
  const handleDrawerOpen = () => {
    setOpenRightMenu(false);
    setOpenLeftMenu(true);
  };

  const handleDrawerClose = () => {
    setOpenLeftMenu(false);
  };

  const classesRightMenu = useStylesRightMenu();
  const themeRightMenu = useTheme();
  const [openRightMenu, setOpenRightMenu] = React.useState(false);

  const handleDrawerOpenRightMenu = () => {
    setOpenLeftMenu(false);
    setOpenRightMenu(true);
  };

  const handleDrawerCloseRightMenu = () => {
    setOpenRightMenu(false);
  };

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const menuId = "primary-search-account-menu";

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton aria-label="show 11 new notifications" color="inherit">
          <Badge badgeContent={11} color="secondary">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <p>Notifications</p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );

  const CustomTextInput = ({ label, ...props }) => {
    const [field, meta] = useField(props);
    return (
      <>
        <label htmlFor={props.id || props.name}>{label}</label>
        <input className="text-input" {...field} {...props} />
        {meta.touched && meta.error ? (
          <div className="error">{meta.error}</div>
        ) : null}
      </>
    );
  };

  return (
    <div className={classesMenu.root}>
      <AppBar
        position="absolute" //fixed
        className={clsx(classesMenu.appBar, classesRightMenu.appBar, {
          [classesMenu.appBarShift]: openLeftMenu,
          [classesRightMenu.appBarShift]: openRightMenu,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            style={{paddingLeft: 20}}
            className={clsx(
              classesMenu.menuButton,
              openLeftMenu && classesMenu.hide
            )}
          >
           <img src={`${BASE_URL}/menu.png`} style={{filter: `brightness(0) invert(1)`, height: 42}}/>
          </IconButton>
          <Typography
            className={classes.title}
            variant="h6"
            noWrap
            style={{ cursor: "pointer" }}
            onClick={() => props.history.push("/home")}
          >
            Bookify
          </Typography>
          {!props.location.pathname.includes("borrowed") &&
            !props.location.pathname.includes("home") && (
              <div className={classes.search}>
                <div className={classes.searchIcon}>
                  <SearchIcon />
                </div>
                <InputBase
                  placeholder="Search…"
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                  }}
                  inputProps={{ "aria-label": "search" }}
                  onKeyPress={(e) => {
                    if (e.key === "Enter") {
                      const name = e.target.value;
                      props.history.push(`/books?title=${name}`);
                    }
                  }}
                />
              </div>
            )}
          <div className={classes.grow} />
          {user ? (
            <div className={classes.sectionDesktop}>
              <IconButton
                edge="end"
                aria-label="account of current user"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleDrawerOpenRightMenu}
                className={clsx(
                  classesRightMenu.menuButton,
                  openRightMenu && classesRightMenu.hide
                )}
                color="inherit"
                style={{ marginRight: "7px" }}
              >
                {user.avatarUrl ? <Avatar src={`${BASE_URL}/${user.avatarUrl}`} alt={user.username[0].toUpperCase()} /> : <Avatar src="empty" alt={user.username[0].toUpperCase()} />}
              </IconButton>
            </div>
          ) : null}
          <Login setOpenRightMenu={setOpenRightMenu} />
        </Toolbar>
      </AppBar>
      <Drawer
        className={classesMenu.drawer}
        variant="persistent"
        anchor="left"
        open={openLeftMenu}
        classes={{
          paper: classesMenu.drawerPaper,
        }}
      >
        <div className={classesMenu.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "ltr" ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <List>
          <ListItem
            button
            key={"Home"}
            onClick={() => {
              props.history.push("/home");
              setOpenLeftMenu(false);
            }}
          >
            <ListItemIcon>
              <HomeRoundedIcon />
            </ListItemIcon>
            <ListItemText primary={"Home"} />
          </ListItem>
          <ListItem
            button
            key={"Books"}
            onClick={() => {
              props.history.push("/books");
              setOpenLeftMenu(false);
            }}
          >
            <ListItemIcon>
              <LocalLibraryRoundedIcon />
            </ListItemIcon>
            <ListItemText primary={"Books"} />
          </ListItem>
        </List>
        <Divider />
        {user ? (
          <List>
            <ListItem
              button
              key={"My Profile"}
              onClick={handleDrawerOpenRightMenu}
            >
              <ListItemIcon>
                <AccountCircleRoundedIcon />
              </ListItemIcon>
              <ListItemText primary={"My Profile"} />
            </ListItem>
            <ListItem
              button
              key={"Borrowed books"}
              onClick={() => {
                props.history.push("/books/borrowed");
                setOpenLeftMenu(false);
              }}
            >
              <ListItemIcon>
                <MenuBookRoundedIcon />
              </ListItemIcon>
              <ListItemText primary={"Borrowed books"} />
            </ListItem>
          </List>
        ) : null}
        {user?.role === "Admin" ? (
          <List>
            <Divider />
            <ListItem
              button
              key={"Admin Panel"}
              onClick={() => {
                props.history.push("/admin");
                setOpenLeftMenu(false);
              }}
            >
              <ListItemIcon>
                <SupervisorAccountIcon />
              </ListItemIcon>
              <ListItemText primary={"Admin Panel"} />
            </ListItem>
          </List>
        ) : null}
      </Drawer>

      <Drawer
        className={classesRightMenu.drawer}
        variant="persistent"
        anchor="right"
        open={openRightMenu}
        classes={{
          paper: classesRightMenu.drawerPaper,
        }}
      >
        <div className={classesRightMenu.drawerHeader}>
          <IconButton onClick={handleDrawerCloseRightMenu}>
            {themeRightMenu.direction === "rtl" ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
          {user?.avatarUrl ? <Avatar src={`${BASE_URL}/${user?.avatarUrl}`} alt={user?.username[0].toUpperCase()} style={{marginLeft: '5px'}} /> : <Avatar src="empty" alt={user?.username[0].toUpperCase()} style={{marginLeft: '5px'}} />}
          <ListItemText primary={user?.username} style={{width: '100px'}}/>
        </div>
        <Divider />
        <List>
          <ListItem button key={"level"}>
            <ListItemIcon style={{ minWidth: "40px" }}>
              <GradeRoundedIcon />
            </ListItemIcon>
            <ListItemText primary={`Level: ${user?.level[0] || ""}`} />
            <img
              src={require(`./../../../../images/levels/level${user?.level[1] || 1}.png`)}
              style={{ height: "20px" }}
            />
          </ListItem>
          <ListItem button key={"role"}>
            <ListItemIcon style={{ minWidth: "40px" }}>
              {user?.role === "Basic" ? (
                <PersonIcon />
              ) : (
                <SupervisorAccountIcon />
              )}{" "}
            </ListItemIcon>
            <ListItemText primary={`Role: ${user?.role || ""}`} />
          </ListItem>
          <Divider />
          <Button variant="contained"
            style={{ marginTop: "10px"}}
            onClick={() => setshowUploadFields(!showUploadFields)}
          >
            {!showUploadFields ? !user?.avatarUrl ? 'Upload Avatar' : 'Change Avatar' : 'Hide Fields'}
          </Button>
          <br/>
          {showUploadFields ? (
            <>
              <Button component="label" style={{marginTop: '10px', marginRight: '20px'}}>
                Upload File
                <input type="file" style={{ display: "none" }} onChange={event => setFiles(Array.from(event.target.files))}/>
              </Button>
              <Button onClick={() => {uploadAvatar(user?.id); setshowUploadFields(!showUploadFields)}} style={{ marginTop: "10px" }}>
                Submit
              </Button>
            </>
          ) : null}
          <Button variant="contained"
            style={{ marginTop: "10px"}}
            onClick={handleModalOpen}
          >
            Change Password
          </Button>
            <>
            <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={modalClasses.modal}
        open={modalOpen}
        onClose={handleModalClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={modalOpen}>
          <div className={modalClasses.paper}>
              <Styles>
              <Formik
                initialValues={{
                  oldPassword: "",
                  newPassword: "",
                  confirmNewPassword: "",
                }}
                validationSchema={Yup.object({
                  oldPassword: Yup.string()
                    .min(8, "Must be at least 8 characters")
                    .max(20, "Must be 20 charasters or less")
                    .required("Required"),
                  newPassword: Yup.string()
                    .min(8, "Must be at least 8 characters")
                    .max(20, "Must be 20 characters or less")
                    .required("Required"),
                  confirmNewPassword: Yup.string()
                    .min(8, "Must be at least 8 characters")
                    .max(20, "Must be 20 characters or less")
                    .required("Required")
                    .oneOf(
                      [Yup.ref("newPassword")],
                      "Both passwords must be the same!"
                    ),
                })}
                onSubmit={(values, { setSubmitting, resetForm }) => {
                    fetch(`${BASE_URL}/users`, {
                      method: "PUT",
                      headers: {
                        "Content-Type": "application/json",
                        "Authorization": `Bearer ${localStorage.getItem('token')}`
                      },
                      body: JSON.stringify({
                        oldPassword: values.oldPassword,
                        newPassword: values.newPassword,
                      }),
                    })
                      .then((r) => r.json())
                      .then((result) => {
                        if (result.error) {
                          setSubmitting(false);
                          resetForm();
                          return toastr.error(result.status);
                        }
                        toastr.success("Password successfully updated!");
                        resetForm();
                        setSubmitting(false);
                        setModalOpen(false);
                      })
                      .catch((error) => {
                        setSubmitting(false);
                        resetForm();
                        return toastr.error(error);
                      });
                  }
                }
              >
                {(props) => (
                  <Form>
                    <CustomTextInput
                      autoFocus
                      label="Old Password"
                      name="oldPassword"
                      type="password"
                      placeholder="Enter your old password"
                    />
                    <CustomTextInput
                      label="New Password"
                      name="newPassword"
                      type="password"
                      placeholder="Enter your new password"
                    />
                    <CustomTextInput
                      label="Confirm New Password"
                      name="confirmNewPassword"
                      type="password"
                      placeholder="Confirm your new password"
                    />
                    <button type="submit">
                      { props.isSubmitting
                          ? "Loading ..."
                          : "Submit"}
                    </button>
                  </Form>
                )}
              </Formik>
            </Styles>
            </div>
        </Fade>
      </Modal>
            </>
        </List>
      </Drawer>
      {renderMobileMenu}
    </div>
  );
};

export default withRouter(Navigation);
