import React, { useContext, useState } from "react";
import * as Yup from "yup";
import { Formik, useField, Form } from "formik";
import { makeStyles } from "@material-ui/core/styles";
import { Styles } from "./Styles";
import { BASE_URL } from "../../../common/constants";
import jwtDecode from "jwt-decode";
import UserContext from "./../../../providers/UserContext";
import * as toastr from "toastr";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import Switch from "@material-ui/core/Switch";
import { FormGroup, FormControlLabel } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const Login = (props) => {
  const { user, setUser } = useContext(UserContext);
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [auth, setAuth] = useState(!!user);
  const [register, setRegister] = useState(false);

  const logout = () => {
    setUser(null);
    setAuth(false);
    props.setOpenRightMenu(false);
    localStorage.removeItem("token");
    toastr.success("User successfully logged out!");
    props.history.push("/home");
  };

  const handleOpen = (event) => {
    if (!!user) {
      logout();
    } else {
      setOpen(true);
    }
  };
  const handleClose = () => {
    setOpen(false);
  };

  const CustomTextInput = ({ label, ...props }) => {
    const [field, meta] = useField(props);
    return (
      <>
        <label htmlFor={props.id || props.name}>{label}</label>
        <input className="text-input" {...field} {...props} />
        {meta.touched && meta.error ? (
          <div className="error">{meta.error}</div>
        ) : null}
      </>
    );
  };

  return (
    <div>
      <FormGroup>
        <FormControlLabel
          control={
            <Switch
              checked={auth}
              onClick={handleOpen}
              aria-label="login switch"
            />
          }
          label={auth ? "Logout" : "Login / Register"}
        />
      </FormGroup>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <Styles>
              <Formik
                initialValues={{
                  username: "",
                  password: "",
                  confirmPassword: "",
                }}
                validationSchema={Yup.object({
                  username: Yup.string()
                    .min(5, "Must be at least 5 characters")
                    .max(15, "Must be 15 charasters or less")
                    .required("Required"),
                  password: Yup.string()
                    .min(8, "Must be at least 8 characters")
                    .max(20, "Must be 20 characters or less")
                    .required("Required"),
                  confirmPassword: Yup.string()
                    .min(8, "Must be at least 8 characters")
                    .max(20, "Must be 20 characters or less")
                    .oneOf(
                      [Yup.ref("password")],
                      "Both passwords must be the same!"
                    ),
                })}
                onSubmit={(values, { setSubmitting, resetForm }) => {
                  if (register) {
                    fetch(`${BASE_URL}/users`, {
                      method: "POST",
                      headers: {
                        "Content-Type": "application/json",
                      },
                      body: JSON.stringify({
                        username: values.username,
                        password: values.password,
                      }),
                    })
                      .then((r) => r.json())
                      .then((result) => {
                        if (result.error) {
                          setSubmitting(false);
                          resetForm();
                          return toastr.error(result.status);
                        }
                        toastr.success("Username successfully created!");
                        resetForm();
                        setSubmitting(false);
                        setRegister(false);
                      })
                      .catch((error) => {
                        setSubmitting(false);
                        resetForm();
                        return toastr.error(error);
                      });
                  } else {
                    fetch(`${BASE_URL}/session`, {
                      method: "POST",
                      headers: {
                        "Content-Type": "application/json",
                      },
                      body: JSON.stringify({
                        username: values.username,
                        password: values.password,
                      }),
                    })
                      .then((r) => r.json())
                      .then((result) => {
                        if (result.error) {
                          setSubmitting(false);
                          resetForm();
                          return toastr.error(result.status);
                        }

                        try {
                          const payload = jwtDecode(result.token);
                          setUser(payload);
                        } catch (e) {
                          setSubmitting(false);
                          resetForm();
                          return toastr.error(e.message);
                        }
                        toastr.success("User successfully logged in!");
                        localStorage.setItem("token", result.token);
                        setOpen(false);
                        setAuth(true);
                        resetForm();
                        setSubmitting(false);
                        props.history.push("/home");
                      })
                      .catch((error) => {
                        setSubmitting(false);
                        resetForm();
                        return toastr.error(error);
                      });
                    
                  }
                }}
              >
                {(props) => (
                  <Form>
                    <h1>{register ? "Register" : "Login"}</h1>
                    <CustomTextInput
                      autoFocus
                      label="Username"
                      name="username"
                      type="text"
                      placeholder="Enter your username"
                    />
                    <CustomTextInput
                      label="Password"
                      name="password"
                      type="password"
                      placeholder="Enter your password"
                    />
                    {register ? (
                      <CustomTextInput
                        label="Confirm Password"
                        name="confirmPassword"
                        type="password"
                        placeholder="Confirm your password"
                      />
                    ) : null}
                    <button type="submit">
                      {register
                        ? props.isSubmitting
                          ? "Loading ..."
                          : "Register"
                        : props.isSubmitting
                        ? "Loading..."
                        : "Login"}
                    </button>
                  </Form>
                )}
              </Formik>
            </Styles>
            <Button onClick={() => setRegister(!register)}>
              {register ? "Login user" : "Register user"}
            </Button>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

export default withRouter(Login);
