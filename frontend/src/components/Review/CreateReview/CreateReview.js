import React from 'react';
import { useField } from 'formik';
import { TextField } from '@material-ui/core';

const CreateReview = ({ label, fromEdit, ...props }) => {
    const [field, meta] = useField(props);
    return (
      <div style={{paddingTop: '30px'}}>
        <TextField
        style={{width: '100%'}}
          id="outlined-textarea"
          label={fromEdit ? "Edit review" : "Write review"}
          placeholder="Placeholder"
          multiline
          variant="outlined"
          rows={5}
          {...field}
            {...props}
        />
        {meta.touched && meta.error ? (
          <div className="error" stye={{color: 'red'}}>{meta.error}</div>
        ) : null}
      </div>
    );
  };

  export default CreateReview