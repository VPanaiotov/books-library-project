import React, { useState, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { IconButton } from '@material-ui/core';
import { red } from "@material-ui/core/colors";
import FavoriteIcon from "@material-ui/icons/Favorite";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import UserContext from '../../../providers/UserContext';
import { BASE_URL } from '../../../common/constants';
import * as toastr from 'toastr';
import EditReview from './EditReview';

const SingleReview = ({ review, toggleFetchReviews, fetchReviews}) => {

    const { user } = useContext(UserContext);

    const [like, setLike] = useState(review.likes.some(like => like.user?.id === user.id));

    const toggleLike = () => {
        fetch(`${BASE_URL}/reviews/${review.id}/votes`, {
            method: 'PUT',
            headers: {
              'Authorization': `Bearer ${localStorage.getItem('token') || ''}`,
            },
          })
            .then(response => {
                if (response.error) {
                    throw new Error(response.error)
                }
                setLike(!like);
                toastr.success(`Review successfully ${like ? 'unliked!' : 'liked!'}`)
            })
            .catch(e => toastr.error(e.message))
        }

    const useStyles = makeStyles((theme) => ({
        root: {
          width: '100%',
          backgroundColor: theme.palette.background.paper,
        },
        inline: {
          display: 'inline',
        },
      }));
   
      const classes = useStyles();

  return (
    <List className={classes.root}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
        <Avatar alt={review.author.username[0].toUpperCase()} src="/static/images/avatar/1.jpg" />
        </ListItemAvatar>
        <ListItemText style={{overflow: 'hidden'}}
          primary={review.content}
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                from {review.author.username}
              </Typography>
            </React.Fragment>
          }
        />
        <IconButton
          aria-label="add to favorites"
          onClick={toggleLike}
          className="noFocus"
          title={like ? "Dislike" : "Like"}
        >
          {like
           ? (<FavoriteIcon style={{ color: red[500] }} fontSize="inherit" />)
           : (<FavoriteBorderIcon fontSize="inherit" />)
           }
        </IconButton>
        {review.author.id === user.id || user.role === 'Admin'
          ? <EditReview review={review} toggleFetchReviews={toggleFetchReviews} fetchReviews={fetchReviews} />
          : null}
      </ListItem>
      <Divider variant="inset" component="li" />
    </List>
  );
};

export default SingleReview;