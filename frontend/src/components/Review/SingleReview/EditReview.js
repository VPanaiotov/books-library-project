import React, { useContext } from 'react';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import DeleteIcon from '@material-ui/icons/Delete';
import { BASE_URL } from '../../../common/constants';
import * as toastr from 'toastr';
import EditIcon from '@material-ui/icons/Edit';

import Dialog from '@material-ui/core/Dialog';
import { Button } from '@material-ui/core';
import UserContext from '../../../providers/UserContext';

import * as Yup from "yup";
import { Formik, Form } from "formik";
import CreateReview from '../CreateReview/CreateReview';

const EditReview = ({ review, toggleFetchReviews, fetchReviews, }) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const { user } = useContext(UserContext);
  const open = Boolean(anchorEl);

  const deleteReview = () => {
    let changePathIfAdmin = user.role === 'Admin' ? "/admin" : "";
    fetch(`${BASE_URL}${changePathIfAdmin}/reviews/${review.id}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
      .then(response => {
          if (response.error) {
              throw new Error(response.error)
          }
          toggleFetchReviews(!fetchReviews)
          toastr.success('Review sucessfully deleted!')
      })
      .catch(e => toastr.error(e.message))
  }

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <IconButton
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        PaperProps={{
        }}
      >
          <div>
          <EditDialog toggleFetchReviews={toggleFetchReviews} fetchReviews={fetchReviews} review={review} handleCloseMenu={handleClose}/>
          </div>
          <IconButton
            aria-label="delete review"
            onClick={() => deleteReview()}
            className="noFocus"
            title="Delete"
          ><DeleteIcon /></IconButton>
      </Menu>
    </div>
  );
}
export default EditReview;


const EditDialog = ({ toggleFetchReviews, fetchReviews, review , handleCloseMenu }) => {
    const [open, setOpen] = React.useState(false);
    const { user } = useContext(UserContext);
    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };
  
    return (
      <div>
        <IconButton
            aria-label="edit review"
            onClick={handleClickOpen}
            className="noFocus"
            title="Edit"
          ><EditIcon /></IconButton>
        <Dialog 
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
         <Formik
                initialValues={{
                  comment: review.content,
                }}
                validationSchema={Yup.object({
                  comment: Yup.string()
                    .min(5, "Must be at least 5 characters")
                    .max(50, "Must be 50 charasters or less")

                })}
                onSubmit={(values, { setSubmitting, resetForm }) => {
                    let changePathIfAdmin = user.role === 'Admin' ? "/admin" : "";
                    fetch(`${BASE_URL}${changePathIfAdmin}/reviews/${review.id}`, {
                        method: 'PUT',
                        headers: {
                          'Content-Type': 'application/json',
                          'Authorization': `Bearer ${localStorage.getItem('token') || ''}`,
                        },
                        body: JSON.stringify({
                        content: values.comment
                      }),
                    })
                      .then((r) => r.json())
                      .then((result) => {
                        if (result.error) {
                         throw new Error(result.error)
                        }
                        toastr.success("Review sucessfully edited!");
                        resetForm();
                        setSubmitting(false);
                        handleCloseMenu();
                        handleClose();
                        toggleFetchReviews(!fetchReviews)
                      })
                      .catch((error) => {
                        setSubmitting(false);
                        resetForm();
                        return toastr.error(error);
                      });
                }}
                >
                {(props) => (
                    <Form style={{width: '560px'}}>
                      <CreateReview
                      autoFocus
                      name="comment"
                      type="text"
                      placeholder="Edit review"
                      fromEdit={true}
                    />
                    <br />
                      <Button variant="outlined" color="primary" size="small" type="submit" >Edit review</Button>
                  </Form>
                )}
              </Formik>
        </Dialog>
      </div>
    );
  }