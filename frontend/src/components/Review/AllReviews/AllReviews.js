import React, { useEffect, useState, useContext } from 'react';
import SingleReview from './../SingleReview/SingleReview';
import { BASE_URL } from '../../../common/constants';
import * as toastr from 'toastr';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';

import { makeStyles } from '@material-ui/core/styles';
import { TextField, Button } from '@material-ui/core';

import * as Yup from "yup";
import { Formik, useField, Form } from "formik";
import CreateReview from '../CreateReview/CreateReview';
import UserContext from '../../../providers/UserContext';

const AllReviews = ({book, readBook}) => {

    const [reviews, setReviews] = useState([])
    const [fetchReviews, toggleFetchReviews] = useState(false);

    useEffect(() => {
        // setLoading(true);
        fetch(`${BASE_URL}/books/${book.id}/reviews`)
          .then(response => response.json())
          .then(result => {
            if (result.error){
                throw new Error(result.message);
            }
            setReviews(result);
        })
          .catch(error => toastr.error(error.message))
          .finally();
      }, [fetchReviews]);
    return (
        <>
        <h1>Reviews</h1>
        <Paper style={{maxHeight: 400, overflow: 'auto', width: 560}}>
                {
                reviews.length === 0 && <div style={{width: '50%', margin: 'auto'}}>The book has no reviews</div> 
                ||
                reviews.sort((a, b) => new Date(a.createdOn) - new Date(b.createdOn)).map(review => <SingleReview key={review.id} review={review} toggleFetchReviews={toggleFetchReviews} fetchReviews={fetchReviews}/>)
                }
        </Paper>
       {readBook && <Formik
                initialValues={{
                  comment: "",
                }}
                validationSchema={Yup.object({
                  comment: Yup.string()
                    .min(5, "Must be at least 5 characters")
                    .max(50, "Must be 50 charasters or less")

                })}
                onSubmit={(values, { setSubmitting, resetForm }) => {
                    fetch(`${BASE_URL}/books/${book.id}/reviews`, {
                        method: 'POST',
                        headers: {
                          'Content-Type': 'application/json',
                          'Authorization': `Bearer ${localStorage.getItem('token') || ''}`,
                        },
                        body: JSON.stringify({
                        content: values.comment
                      }),
                    })
                      .then((r) => r.json())
                      .then((result) => {
                        if (result.error) {
                         throw new Error(result.error)
                        }
                        toastr.success("Review successfully created!");
                        resetForm();
                        setSubmitting(false);
                        toggleFetchReviews(!fetchReviews)
                      })
                      .catch((error) => {
                        setSubmitting(false);
                        resetForm();
                        return toastr.error(error);
                      });
                }}
                >
                {(props) => (
                    <Form style={{width: '560px'}}>
                      <CreateReview
                      autoFocus
                      name="comment"
                      type="text"
                      placeholder="Enter review"
                    />
                    <br />
                      <Button variant="outlined" color="primary" size="small" type="submit">Submit review</Button>
                  </Form>
                )}
              </Formik>}
        </>
    );
};

export default AllReviews;
