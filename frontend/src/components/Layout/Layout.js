import React, { Fragment } from 'react';
import Navigation from '../Base/Navigation/Navigation';
import Loading from '../Base/Loading/Loading';

const Layout = props => {
    return (
    <Fragment>
        <Navigation />
        <main>{props.children}</main>
    </Fragment>
    );
};

export default Layout;