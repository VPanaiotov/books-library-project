import React, { useState, useEffect, useContext } from 'react';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import * as toastr from 'toastr';
import PropTypes from 'prop-types';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import SentimentDissatisfiedIcon from '@material-ui/icons/SentimentDissatisfied';
import SentimentSatisfiedIcon from '@material-ui/icons/SentimentSatisfied';
import SentimentSatisfiedAltIcon from '@material-ui/icons/SentimentSatisfiedAltOutlined';
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import { BASE_URL } from '../../common/constants';
import UserContext from '../../providers/UserContext';
import { Button } from '@material-ui/core';

const Rate = ({readBook, id}) => {
  const [rates, setRates] = useState([]);
  const [avrRate, setAvrRate] = useState(0);
  const { user } = useContext(UserContext);

  useEffect(() => {
    // setLoading(true);

    fetch(`${BASE_URL}/books/${id}/rates`)
      .then(response => response.json())
      .then(result => {
        if (Array.isArray(result)) {
          setRates(result)
          setAvrRate((result.reduce((acc, rate) => (acc + rate?.rate), 0) / result.length) || 0)
        }
      })
      .catch(error => toastr.error(error.message))
      .finally(() => {});
  }, []);

const [clickRate, setClickRate] = useState(false);

  const [value, setValue] = React.useState(2);
  const [hover, setHover] = React.useState(-1);


  const customIcons = {
    1: {
      icon: <SentimentVeryDissatisfiedIcon />,
      label: 'Very Dissatisfied',
    },
    2: {
      icon: <SentimentDissatisfiedIcon />,
      label: 'Dissatisfied',
    },
    3: {
      icon: <SentimentSatisfiedIcon />,
      label: 'Neutral',
    },
    4: {
      icon: <SentimentSatisfiedAltIcon />,
      label: 'Satisfied',
    },
    5: {
      icon: <SentimentVerySatisfiedIcon />,
      label: 'Very Satisfied',
    },
  };
  
  function IconContainer(props) {
    const { value, ...other } = props;
    return <span {...other}>{customIcons[value].icon}</span>;
  }
  
  IconContainer.propTypes = {
    value: PropTypes.number.isRequired,
  };

  const handleNewRate = (newHover) => {
    fetch(`${BASE_URL}/books/${id}/rates`, {
      method: 'PUT',
      headers: {
          'Authorization': `Bearer ${localStorage.getItem('token') || ''}`,
          'Content-Type': 'application/json'
      },
      body: JSON.stringify({rate: newHover})
  })
      .then(response => response.json())
      .then(result => {
        if (result.error) {
          throw new Error(result.error)
        }
        fetch(`${BASE_URL}/books/${id}/rates`)
      .then(response => response.json())
      .then(result => {
        if (!result.error) {
          setRates(result)
          setAvrRate((result.reduce((acc, rate) => (acc + rate?.rate), 0) / result.length) || 0)
        } else {
          return toastr.error(result.message);
        }
      })
      .catch(error => toastr.error(error.message))
      .finally(() => {});
        toastr.success(result.message);
      })
      .catch(error => toastr.error(error.message))
      .finally(() => {});
  }

  return (
    <div>
      <Box component="fieldset" mb={3} borderColor="transparent">
        <Rating
          name="customized-icons"
          value={clickRate && 1 || avrRate}
          readOnly={!clickRate}
          getLabelText={(value) => customIcons[value]?.label}
          IconContainerComponent={IconContainer}
          onChange={(event, newValue) => {
            setValue(event.target.value);
            setClickRate(false)
            handleNewRate(hover)
          }}
          onChangeActive={(event, newHover) => {
            setHover(newHover);
          }}
        />
        {value !== null && <Box ml={2}>{customIcons[hover]?.label}</Box>}
        {readBook && !clickRate && <Button variant="outlined" color="primary" size="small" onClick={() => setClickRate(true)}>Rate</Button>}
      </Box>
    </div>
  );
}

export default Rate;