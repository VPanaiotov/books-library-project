import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { BASE_URL } from '../../common/constants';
import * as toastr from 'toastr';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const BookCoverUploadPreview = ({book, state, setState}) =>  {
  const [open, setOpen] = React.useState(true);

  const handleClose = () => {
      const file = new FormData()
      file.append('files', book.file)

    fetch(`${BASE_URL}/admin/books/${book.book.id}/cover`, {
        method: "POST",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
        },
        body: file
      })
        .then((r) => r.json())
        .then((result) => {
          if (result.error) {
            return toastr.error(result.message);
          }
          setState((prevState) => {
            const data = [...prevState.data];
            data[data.indexOf(book.book)] = {...book.book, coverUrl: result.coverUrl};
            return { ...prevState, data };
          });
          toastr.success('Book cover successfully uploaded!');
        })
        .catch((error) => toastr.error(error.message))
        .finally(() => setOpen(false))
    
  };

  return (

      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">{book.book.title}</DialogTitle>
        <DialogContent>
          <img src={book.imgUrl} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Upload
          </Button>
          <Button onClick={() => setOpen(false)} color="primary">
            Discard
          </Button>
        </DialogActions>
      </Dialog>
  )
}

export default BookCoverUploadPreview;