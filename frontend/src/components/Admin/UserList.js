import React, { useEffect, useState, useContext } from "react";
import { BASE_URL } from "../../common/constants";
import * as toastr from "toastr";
import MaterialTable from "material-table";
import RestoreIcon from "@material-ui/icons/Restore";
import DeleteIcon from "@material-ui/icons/Delete";
import UserContext from "../../providers/UserContext";
import { withRouter } from "react-router-dom";
import Loading from "../Base/Loading/Loading";
import { Avatar } from "@material-ui/core";

const UsersList = (props) => {
  const [showLoading, toggleLoading] = useState(false);
  const { user } = useContext(UserContext);
  const [state, setState] = useState({
    columns: [
      {
        title: "Avatar",
        field: "avatarUrl",
        render: (rowData) => (
          
          <Avatar src={`${BASE_URL}/${rowData.avatarUrl}`} alt={rowData.username[0].toUpperCase()} style={{marginLeft: '15px'}} />
        ),
        editable: "never",
      },
      { title: "Id", field: "id", editable: "never" },
      { title: "Username", field: "username", editable: "onAdd" },
      {
        title: "Deleted",
        field: "isDeleted",
        lookup: { true: "true", false: "false" },
        editable: "never",
      },
      {
        title: "Level",
        field: "level",
        lookup: {
          1: "New",
          2: "Advancing",
          3: "Moderate",
          4: "HeavyReader",
          5: "Kokos",
        },
        editable: "never",
      },
      { title: "Points", field: "points", editable: "never" },
      {
        title: "Role",
        field: "role",
        lookup: { 1: "Basic", 2: "Admin" },
        editable: "onUpdate",
      },
      {
        title: "Ban end date",
        field: "banEndDate",
        type: "date",
        editable: "onUpdate",
      },
      {
        title: "Ban description",
        field: "banDescription",
        editable: "onUpdate",
      },
    ],
    data: [],
  });

  useEffect(() => {
    toggleLoading(true);
    fetch(`${BASE_URL}/admin/users`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (Array.isArray(result) && result.length !== 0) {
          setState({
            ...state,
            data: result
              .filter((userIn) => userIn.id !== user.id)
            });
        }
      })
      .catch((error) => toastr.error(error.message))
      .finally(() => toggleLoading(false));
  }, []);

  return (
    <>
    {showLoading && <Loading />}
      <MaterialTable
        title="User list"
        columns={state.columns}
        data={state.data}
        options={{
          rowStyle: (x) => {
            if (x.isDeleted) {
              return { backgroundColor: "#ec407a" };
            }
            if (x.banEndDate) {
              return { backgroundColor: "#ffeb3b" };
            }
          },
          pageSize: 10,
          pageSizeOptions: [20, 30, 50],
          actionsColumnIndex: -1,
        }}
        actions={[
          (rowData) => ({
            icon: rowData.isDeleted ? RestoreIcon : DeleteIcon,
            tooltip: rowData.isDeleted ? "Restore" : "Delete",
            onClick: (event, rowData) => {
              toggleLoading(true);
              return fetch(`${BASE_URL}/admin/users/${rowData.id}`, {
                method: rowData.isDeleted ? "PATCH" : "DELETE",
                headers: {
                  "Content-Type": "application/json",
                  Authorization: `Bearer ${
                    localStorage.getItem("token") || ""
                  }`,
                },
              })
                .then((r) => r.json())
                .then((result) => {
                  if (result.error) {
                    return toastr.error(result.message);
                  }
                  toastr.success(
                    result.message === `User ${rowData.username} deleted!`
                      ? "User successfully deleted!"
                      : "User successfully restored!"
                  );
                  setState((prevState) => {
                    const data = [...prevState.data];
                    const index = data.indexOf(rowData);
                    data[index].isDeleted = !data[index].isDeleted;
                    return { ...prevState, data };
                  });
                })
                .catch((error) => toastr.error(error.message))
                .finally(() => toggleLoading(false));
            }
          }),
        ]}
        editable={{
          onRowAdd: (newData) =>{
            console.log(newData)
            toggleLoading(true);
            return fetch(`${BASE_URL}/admin/users`, {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
              },
              body: JSON.stringify({
                username: newData.username,
                password: "pa$$w0rd",
              }),
            })
              .then((r) => r.json())
              .then((result) => {
                if (result.error) {
                  return toastr.error(result.status);
                }
              setState((prevState) => {
                const data = [...prevState.data,{
                          ...newData,
                          isDeleted: false,
                          level: 1,
                          role: 1,
                          points: 0,
                          id: result.id,
                        }];
                return { ...prevState, data };
              });
              toastr.success("Username successfully created!");
              })
              .catch((error) => {
                return toastr.error(error);
              })
              .finally(() => toggleLoading(false));
            },

          onRowUpdate: (newData, oldData) => {
                if(newData.role !== oldData.role) {
                  toggleLoading(true)
                return fetch(`${BASE_URL}/admin/users/${oldData.id}`, {
                    method: "PUT",
                    headers: {
                      "Content-Type": "application/json",
                      Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
                    },
                    body: JSON.stringify({
                      ...newData,
                      role: newData.role === "1" ? 1 : 2,
                    }),
                  })
                    .then((r) => r.json())
                    .then((result) => {
                      if (result.error) {
                        return toastr.error(result.message);
                      }
                      if(newData.banEndDate !== oldData.banEndDate || newData.banDescription !== oldData.banDescription) {
                        toastr.info('You cannot edit the role of the user along with the ban properties, only role is updated!');
                      }
                      else toastr.success("User role successfully updated!");
                      setState((prevState) => {
                        const data = [...prevState.data];
                        data[data.indexOf(oldData)] = newData;
                        data[data.indexOf(newData)].banEndDate = oldData.banEndDate;
                        data[data.indexOf(newData)].banDescription = oldData.banDescription;
                        return { ...prevState, data };
                      });
                    })
                    .catch((error) => toastr.error(error.message))
                    .finally(() => toggleLoading(false));
            }
            
            else {
                const banPeriod = Date.parse(newData.banEndDate)-Date.parse(new Date(Date.now()));
                return fetch(`${BASE_URL}/admin/users/${oldData.id}/ban`, {
                    method: banPeriod < 0 ? "DELETE" : "POST",
                    headers: {
                      "Content-Type": "application/json",
                      Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
                    },
                    body: JSON.stringify({
                      period: banPeriod,
                      description: newData.banDescription
                    }),
                  })
                    .then((r) => r.json())
                    .then((result) => {
                      if (result.error) {
                        return result.error === 'NOT_FOUND' ? toastr.error('Cannot ban a user that is deleted!') : toastr.error(result.message);
                      }
                      if (newData.role !== oldData.role) {
                          toastr.info('You cannot change the role and the ban properties simultaneosly, only ban is updated!');
                      }
                      else toastr.success(oldData.banEndDate ? banPeriod < 0 ? "Ban successfully removed!" : "Ban successfully updated!" : "User successfully banned!");
                      setState((prevState) => {
                        const data = [...prevState.data];
                        data[data.indexOf(oldData)] = newData;
                        if(banPeriod < 0) {
                            data[data.indexOf(newData)].role = oldData.role;
                            data[data.indexOf(newData)].banEndDate = null;
                            data[data.indexOf(newData)].banDescription = null;
                        }
                        return { ...prevState, data };
                      });
                    })
                    .catch((error) => toastr.error(error.message))
                    .finally(() => toggleLoading(false));
            }
            
            }
        }}
      />
    </>
  );
};

export default withRouter(UsersList);
