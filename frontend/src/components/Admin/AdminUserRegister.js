import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { BASE_URL } from '../../common/constants';
import * as toastr from 'toastr';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const AdminUserRegister = ({toggleShowRegister, toggleRerender, rerender}) => {
  const classes = useStyles();

  const [values, setValues] = useState({
      username: '',
      password: 'pa$$w0rd'
  })

  const handleRegister = () => {
    fetch(`${BASE_URL}/admin/users`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          'Authorization': `Bearer ${localStorage.getItem('token') || ''}`
        },
        body: JSON.stringify({
          username: values.username,
          password: values.password,
        }),
      })
        .then((r) => r.json())
        .then((result) => {
          if (result.error) {
            return toastr.error(result.status);
          }
          toastr.success("Username successfully created!");
          toggleRerender(!rerender);
        //   setState({...state, data: [...data, ]})
          
          toggleShowRegister(false);
        })
        .catch((error) => {
          return toastr.error(error);
        });
}

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Register
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoFocus
            value={values.username}
            onChange={(e, value)=> setValues({...values, username: e.target.value})}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="text"
            id="password"
            value={values.password}
            disabled
            autoComplete="current-password"
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="confirm password"
            label="Confirm Password"
            type="text"
            value={values.password}
            disabled
            id="confirmpassword"
            autoComplete="current-password"
          />
          <Button variant="outlined" color="primary"
          onClick={handleRegister}
          >
           Register
          </Button>
        </form>
      </div>
    </Container>
  );
}

export default AdminUserRegister;