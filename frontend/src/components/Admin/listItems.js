import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AccountCircleRoundedIcon from '@material-ui/icons/AccountCircleRounded';
import MenuBookRoundedIcon from '@material-ui/icons/MenuBookRounded';
import HomeRoundedIcon from '@material-ui/icons/HomeRounded';
import { withRouter } from 'react-router-dom';

const MainListItems = ({setType, history}) => {

  return (
    <div>
    <ListItem button onClick={() => history.push('/home')}>
      <ListItemIcon>
      <HomeRoundedIcon />
      </ListItemIcon>
      <ListItemText primary="Back to Home" />
    </ListItem>
    <ListItem button onClick={() => setType('books')}>
      <ListItemIcon>
      <MenuBookRoundedIcon />
      </ListItemIcon>
      <ListItemText primary="Books" />
    </ListItem>
    <ListItem button onClick={() => setType('users')}>
      <ListItemIcon>
      <AccountCircleRoundedIcon />
      </ListItemIcon>
      <ListItemText primary="Users" />
    </ListItem>
  </div>
  );
  
}
export default withRouter(MainListItems);