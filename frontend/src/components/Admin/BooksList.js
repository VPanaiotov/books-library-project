import React, { useEffect, useState } from "react";
import { BASE_URL } from "../../common/constants";
import * as toastr from "toastr";
import MaterialTable from "material-table";
import RestoreIcon from "@material-ui/icons/Restore";
import DeleteIcon from "@material-ui/icons/Delete";
import Loading from './../Base/Loading/Loading'
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import { Button } from "@material-ui/core";
import BookCoverUploadPreview from "./BookCoverUpload";

const BooksList = () => {

  const [showLoading, toggleLoading] = useState(false);
  const [currentBook, setCurrentBook] = useState({
    book: null,
    imgUrl: null,
    file: null,
  });
  const [state, setState] = useState({
    columns: [
      {
        title: "Cover",
        field: "coverUrl",
        render: (rowData) => (
          <img  src={`${BASE_URL}/${rowData.coverUrl}`} alt={'NO COVER'} style={{width: 120, height: 230}} />
        ),
        editable: "never",
      },
      { title: "Id", field: "id", editable: "never", cellStyle: {
        width: 20,
        minWidth: 20,
        maxWidth: 20
      },
      headerStyle: {
        width:20,
        minWidth: 20,
        maxWidth: 20
      }},
      { title: "Title", field: "title" },
      { title: "Author", field: "author" },
      { title: "Description", field: "description", cellStyle: {
        width: 250,
        minWidth: 250,
        maxWidth: 250
      },
      headerStyle: {
        width:250,
        minWidth: 250,
        maxWidth: 250
      }},
      {
        title: "Unlisted",
        field: "isUnlisted",
        lookup: { true: "true", false: "false" },
        editable: "never",
        cellStyle: {
          width: 20,
          minWidth: 20,
          maxWidth: 20
        },
        headerStyle: {
          width:20,
          minWidth: 20,
          maxWidth: 20
        }
      },
      { title: "Borrowed By", field: "borrowedBy", editable: "never" },
      { title: "Average Rate", field: "rate", editable: "never", cellStyle: {
        width: 40,
        minWidth: 40,
        maxWidth: 40
      },
      headerStyle: {
        width:40,
        minWidth: 40,
        maxWidth: 40
      } },
    ],
    data: [],
  });

  useEffect(() => {
    toggleLoading(true);
    fetch(`${BASE_URL}/admin/books`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (Array.isArray(result) && result.length !== 0) {
          setState({
            ...state,
            data: result
              .sort((a, b) => a.id - b.id)
              .map((book) => ({
                ...book,
                borrowedBy: book.borrowedBy?.username,
                rate:
                  book.rates.reduce((acc, rate) => acc + rate?.rate, 0) /
                    book.rates.length || 0,
              })),
          });
        }
      })
      .catch((error) => toastr.error(error.message))
      .finally(() => toggleLoading(false));
  }, []);

  const handleCoverUpload = (input) => {
    if(!input){
      return
    }
    setCurrentBook({
      ...currentBook,
      imgUrl: URL.createObjectURL(input),
      file: input
    })
      }

  const UploadCoverIcon = () => (
    <Button 
    style={{ backgroundColor: 'transparent' }}
    className='btn'
      component="label"
    >
      <AddAPhotoIcon />
      <input
        type="file"
        onChange={(e) => handleCoverUpload(e.target.files[0])}
        style={{ display: "none" }}
      />
    </Button>);

  return (
    <>
    <MaterialTable
      title="Books List"
      columns={state.columns}
      data={state.data}
      actions={[
        (rowData) => ({
          icon: rowData.isUnlisted ? RestoreIcon : DeleteIcon,
          tooltip: rowData.isUnlisted ? "Restore" : "Delete",
          onClick: (event, rowData) => {
          toggleLoading(true);
            return fetch(`${BASE_URL}/admin/books/${rowData.id}`, {
              method: "DELETE",
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
              },
            })
              .then((r) => r.json())
              .then((result) => {
                if (result.error) {
                  return toastr.error(result.message);
                }
                toastr.success(
                  result.message === "Book unlisted"
                    ? "Book successfully unlisted!"
                    : "Book successfully restored!"
                );
                setState((prevState) => {
                  const data = [...prevState.data];
                  const index = data.indexOf(rowData);
                  data[index].isUnlisted = !data[index].isUnlisted;
                  return { ...prevState, data };
                });
              })
              .catch((error) => toastr.error(error.message))
              .finally(() => toggleLoading(false))
        }}),
        (rowData) => ({
          icon: UploadCoverIcon,
          tooltip: rowData.coverUrl ? "Change Cover" : "Upload Cover",
          onClick: (event, rowData) => {
            setCurrentBook({
              ...currentBook,
              book: rowData
            })
          }
        })
      ]}
      editable={{
        onRowAdd: (newData) => {
          toggleLoading(true);
          return fetch(`${BASE_URL}/admin/books/`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
            },
            body: JSON.stringify({ ...newData }),
          })
            .then((r) => r.json())
            .then((result) => {
              if (result.error) {
                return toastr.error(result.message);
              }
              toastr.success("Book successfully created!");
              setState((prevState) => {
                const data = [...prevState.data];
                const newDataFromServer = {
                  ...newData,
                  id: result.id,
                  isUnlisted: false,
                  rate: 0,
                  borrowedBy: "",
                };
                data.push(newDataFromServer);
                return { ...prevState, data };
              });
            })
            .catch((error) => toastr.error(error.message))
            .finally(() => toggleLoading(false))
          },

        onRowUpdate: (newData, oldData) => {
          toggleLoading(true);
          return fetch(`${BASE_URL}/admin/books/${oldData.id}`, {
            method: "PUT",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
            },
            body: JSON.stringify({ ...newData }),
          })
            .then((r) => r.json())
            .then((result) => {
              if (result.error) {
                return toastr.error(result.message);
              }
              toastr.success("Book successfully edited!");
              if (oldData) {
                setState((prevState) => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            })
            .catch((error) => toastr.error(error.message))
            .finally(() => toggleLoading(false))
        }
      }}
      options={{
        actionsColumnIndex: -1,
        rowStyle: (x) => {
          if (x.isUnlisted) {
            return { backgroundColor: "#ec407a" };
          }
          if (currentBook.imgUrl && x.id === currentBook.book.id) {
            return { backgroundColor: "#8bc34a" };
          }
        },
        pageSize: 10,
        pageSizeOptions: [20, 30, 50],
      }}
    />
    {showLoading && <Loading />}
    {currentBook.imgUrl && <BookCoverUploadPreview book={currentBook} state={state} setState={setState} />}
    </>
  );
};

export default BooksList;
