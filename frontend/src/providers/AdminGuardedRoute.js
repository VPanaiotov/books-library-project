import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from  'prop-types';

const AdminGuardedRoute = ({ component: Component, role, ...rest }) => (
  <Route
    {...rest} render={(props) => (
        role === "Admin"
            ? <Component {...props} />
            : <Redirect to='/home' />
    )}
  />
);

AdminGuardedRoute.propTypes = {
  component: PropTypes.func.isRequired,
};

export default AdminGuardedRoute;