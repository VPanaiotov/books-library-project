import React, { useState } from "react";
import "./App.css";
import AllBooks from "./components/Book/AllBooks/AllBooks";
import Home from "./components/pages/Home/Home";
import { BrowserRouter, Switch, Redirect, Route } from "react-router-dom";
import UserContext, { getLoggedUser } from "./providers/UserContext";
import Layout from "./components/Layout/Layout";
import "./common/toastr-service";
import AdminDashboard from "./components/Admin/Dashboard";
import GuardedRoute from "./providers/GuardedRoute";
import AdminGuardedRoute from "./providers/AdminGuardedRoute";
import Loading from "./components/Base/Loading/Loading";

const App = () => {
  const [user, setUser] = useState(getLoggedUser());

  return (
    <div className="App">
      <BrowserRouter>
        <UserContext.Provider value={{ user, setUser }}>
          <Layout>
            <Switch>
              <Redirect from="/" exact to="/home" />
              <Route path="/home" component={Home} />
              <GuardedRoute path="/books" auth={!!user} component={AllBooks} />
              <GuardedRoute path="/books/borrowed" exact auth={!!user} component={AllBooks} />
              {/* <Route path="/reviews/:id" component={SingleReview} />
              <Route path="/admin/users" component={AllUsers} />
              <Route path="/admin/users/:id" component={SingleUser} />
              <Route path="/users/:id" component={SingleUser} />
              <Route path="*" component={NotFound} /> */}
              <AdminGuardedRoute path="/admin" role={user?.role} component={AdminDashboard} />
            </Switch>
          </Layout>
        </UserContext.Provider>
      </BrowserRouter>
    </div>
  );
};

export default App;
